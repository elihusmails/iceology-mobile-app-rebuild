import React, { useContext } from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'

import OnboardingNavigator from '../navigation/OnboardingNavigator'
import AthleteNavigator from '../navigation/AthleteNavigator'
import CoachNavigator from '../navigation/CoachNavigator'

import { AuthContext } from '../navigation/AuthProvider'

const HomeStack = () => {
  const { onboarded, user, role } = useContext(AuthContext)

  console.log('HomeStack: navigator negotiation')
  console.log({ user })
  console.log({ role })
  
  switch (role) {
    case 'coach':
      return CoachNavigator
      break
    case 'athlete':
    default:
      return !onboarded ? OnboardingNavigator : AthleteNavigator  
      break
  }
}

export default HomeStack
