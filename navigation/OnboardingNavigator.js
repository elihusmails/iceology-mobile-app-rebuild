import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'

import ProfileScreen from '../screens/Onboarding/ProfileScreen'
import SubscribeScreen from '../screens/Onboarding/SubscribeScreen'
import { navigationOptions, tabBarOptions } from '../shared/navigation'

const Stack = createStackNavigator()
const OnboardingNavigator = (
  <Stack.Navigator initialRouteName='OnboardingProfile' screenOptions={navigationOptions}>
    <Stack.Screen
      name='OnboardingProfile'
      component={ProfileScreen}
      options={{ headerTitle: 'Create your profile' }} />
    <Stack.Screen
      name='OnboardingSubscribe'
      component={SubscribeScreen}
      options={{ headerTitle: 'Subscribe' }} />
  </Stack.Navigator>
)

export default OnboardingNavigator
