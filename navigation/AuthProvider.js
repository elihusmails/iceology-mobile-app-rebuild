import React, { createContext, useState, useEffect } from 'react'
import auth from '@react-native-firebase/auth'
import { useAsyncStorage } from '@react-native-async-storage/async-storage'

import { firebaseGetUser, firebaseCreateUser, firebaseUpdateUserOnboarded, firebaseFormatError } from '../shared/firebase'

export const AuthContext = createContext({})

export const AuthProvider = ({ children }) => {
  const [user, setUser] = useState(null)
  const [role, setRole] = useState(null)
  const [error, setError] = useState(null)

  const [welcomed, setWelcomed] = useState(false)
  const [onboarded, setOnboarded] = useState(null)

  const { getItem: getWelcomedAsyncStorage, setItem: setWelcomedAsyncStorage } = useAsyncStorage("@welcomed")
  
  const readWelcomedFromStorage = async () => {
    return JSON.parse(await getWelcomedAsyncStorage())
  }
  
  const writeWelcomedToStorage = async value => {
    await setWelcomedAsyncStorage(JSON.stringify(value))
  }
  
  const restoreWelcomedFromStorage = async () => {
    setWelcomed(await readWelcomedFromStorage())
  }
  
  const saveWelcomed = (state) => {
    setWelcomed(state)
  }
  
  const saveOnboarded = (state) => {
    firebaseUpdateUserOnboarded(user.uid, state)
    setOnboarded(state)
  }
  
  useEffect(() => {
    restoreWelcomedFromStorage()
  }, []) // Run on first render only

  useEffect(() => {
    writeWelcomedToStorage(welcomed)
  }, [welcomed])

  //useEffect(() => {
  //  // Store the `onboarded` status in Firebase instead of the device's storage.
  //  // Otherwise, the app will incorrectly skip over the welcome and onboarding screens if a user
  //  // has previously navigated through these screens with a different account on the same device.
  //  saveOnboarded(onboarded)
  //}, [onboarded])
  
  return (
    <AuthContext.Provider
      value={{
        welcomed, setWelcomed, saveWelcomed,
        onboarded, setOnboarded, saveOnboarded,
        user, setUser,
        role, setRole,
        error, setError,
        login: async (email, password) => {
          try {
            const userCredential = await auth().signInWithEmailAndPassword(email, password)
            const _user = await firebaseGetUser(userCredential.user.uid)
            setRole(_user.role)
            setError(null) // Reset any errors so they don't redisplay if we log out and go back to the login form
          } catch (e) {
            setError(firebaseFormatError(e))
          }
        },
        register: async (email, password) => {
          try {
            const userCredential = await auth().createUserWithEmailAndPassword(email, password)
            await firebaseCreateUser(userCredential.user.uid)
            setRole('athlete') // Only athletes can register
          } catch (e) {
            setError(firebaseFormatError(e));
          }
        },
        forgotPassword: async (email) => {
          try {
            await auth().sendPasswordResetEmail(email);
          } catch (e) {
            setError(firebaseFormatError(e));
          }
        },
        logout: async () => {
          try {
            await auth().signOut();
          } catch (e) {
            setError(firebaseFormatError(e));
          }
        }
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};
