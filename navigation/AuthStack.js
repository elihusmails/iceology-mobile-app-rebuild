import React, { useContext } from 'react'
import { createStackNavigator } from '@react-navigation/stack'

import WelcomeScreen from '../screens/WelcomeScreen'
import LoginScreen from '../screens/Auth/LoginScreen'
import RegisterScreen from '../screens/Auth/RegisterScreen'
import ForgotPasswordScreen from '../screens/Auth/ForgotPasswordScreen'
import { AuthContext } from '../navigation/AuthProvider'

const Stack = createStackNavigator()

const WelcomeNavigator = (
  <Stack.Navigator screenOptions={{ header: () => null }}>
    <Stack.Screen name='Welcome' component={WelcomeScreen} />
  </Stack.Navigator>
)

const AuthNavigator = (
  <Stack.Navigator initialRouteName='Register'
    screenOptions={{
      headerTransparent: true,
      headerTintColor: '#ffffff',
      headerTitle: null,
    }}>
    <Stack.Screen name='Login' component={LoginScreen} options={{ header: () => null }} />
    <Stack.Screen name='Register' component={RegisterScreen} options={{ header: () => null }} />
    <Stack.Screen name='ForgotPassword' component={ForgotPasswordScreen} options={{ header: () => null }} />
  </Stack.Navigator>
)

export default function AuthStack() {
  const { welcomed } = useContext(AuthContext)
  
  return !welcomed ? WelcomeNavigator : AuthNavigator
}
