import React, { useContext, useState, useEffect } from 'react'
import { StatusBar } from 'react-native'
import { NavigationContainer } from '@react-navigation/native'
import auth from '@react-native-firebase/auth'
import Purchases from 'react-native-purchases'

import AuthStack from './AuthStack'
import HomeStack from './HomeStack'
import { firebaseCreateUser, firebaseGetUser } from '../shared/firebase'
import { AuthContext } from './AuthProvider'
import Loading from '../components/Loading'

export default function Routes() {
  const { user, setUser, role, setRole, setOnboarded, logout } = useContext(AuthContext)
  const [loading, setLoading] = useState(true)
  const [initializing, setInitializing] = useState(true)
  
  async function onAuthStateChanged(user) {
    console.log('onAuthStateChanged')
    setUser(user)

    // Identify the logged-in user to RevenueCat:
    // https://docs.revenuecat.com/docs/user-ids
    if (user) {
      //Purchases.identify(user.uid)
      const purchaserInfo = await Purchases.identify(user.uid)
    } else {
      //Purchases.reset()
      await Purchases.reset()
    }

    if (user) {
      const _user = await firebaseGetUser(user.uid)
      //console.log({_user})
      setRole(_user.role)
      setOnboarded(_user.onboarded)
    } else {
      setRole(null)
      setOnboarded(null)
    }
        
    if (initializing) setInitializing(false)
    setLoading(false)
  }
  
  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged)
    return subscriber // unsubscribe on unmount
  }, [])
  
  if (loading) {
    return <Loading />
  }
  
  return (
    <NavigationContainer>
      <StatusBar barStyle="dark-content" />
      
      {(user && role) ? <HomeStack /> : <AuthStack />}
    </NavigationContainer>
  )
}
