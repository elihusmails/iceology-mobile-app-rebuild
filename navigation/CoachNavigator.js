import React from 'react'
import { Button, View, Text } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'

import VideosListScreen from '../screens/Coach/Videos/VideosListScreen'
import VideoDetailScreen from '../screens/Coach/Videos/VideoDetailScreen'
import VideoFeedbackScreen from '../screens/Coach/Videos/VideoFeedbackScreen'
import VideoFollowupScreen from '../screens/Coach/Videos/VideoFollowupScreen'
import ProfileScreen from '../screens/Coach/Profile/ProfileScreen'
import ProfileEditScreen from '../screens/Coach/Profile/ProfileEditScreen'
import TabBarIcon from '../components/TabBarIcon'
import { navigationOptions, tabBarOptions } from '../shared/navigation'

const Stack = createStackNavigator()
const Tab = createBottomTabNavigator()

const VideosNavigator = () => (
  <Stack.Navigator screenOptions={navigationOptions}>
    <Stack.Screen name='CoachVideosList' component={VideosListScreen} options={{ title: "Videos" }} />
    <Stack.Screen name='CoachVideoDetail' component={VideoDetailScreen} options={{ title: "Video Details" }} />
    <Stack.Screen name='CoachVideoFeedback' component={VideoFeedbackScreen} options={{ title: "Video Feedback" }} />
    <Stack.Screen name='CoachVideoFollowup' component={VideoFollowupScreen} options={{ title: "Video Followup" }} />
  </Stack.Navigator>
)

const ProfileNavigator = () => (
  <Stack.Navigator screenOptions={navigationOptions}>
    <Stack.Screen name='CoachProfile' component={ProfileScreen} options={{ title: "Profile" }} />
    <Stack.Screen name='CoachProfileEdit' component={ProfileEditScreen} options={{ title: "Edit Your Profile" }} />
  </Stack.Navigator>
)

const CoachMain = () => (
  <Tab.Navigator
    tabBarOptions={tabBarOptions}
    >
    <Tab.Screen
      name="Videos"
      component={VideosNavigator}
      options={{tabBarIcon: ({focused}) => (
        <TabBarIcon icon="videos" focused={focused} />
      )}} />
    <Tab.Screen
      name="Profile"
      component={ProfileNavigator}
      options={{tabBarIcon: ({focused}) => (
        <TabBarIcon icon="profile" focused={focused} />
      )}} />
  </Tab.Navigator>
)

const CoachNavigator = (
  <Stack.Navigator mode="modal" screenOptions={navigationOptions} headerMode="none">
    <Stack.Screen name="CoachMain" component={CoachMain} />
  </Stack.Navigator>
)

export default CoachNavigator
