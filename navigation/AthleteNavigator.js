import React from 'react'
import { Button, View, Text } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'

import VideosListScreen from '../screens/Athlete/Videos/VideosListScreen'
import VideoDetailScreen from '../screens/Athlete/Videos/VideoDetailScreen'
import VideoFollowupScreen from '../screens/Athlete/Videos/VideoFollowupScreen'
import NewVideoScreen from '../screens/Athlete/Videos/NewVideoScreen'
import NewVideoRecordScreen from '../screens/Athlete/Videos/NewVideoRecordScreen'
import NewVideoPickerScreen from '../screens/Athlete/Videos/NewVideoPickerScreen'
import NewVideoConfirmScreen from '../screens/Athlete/Videos/NewVideoConfirmScreen'
import ProfileScreen from '../screens/Athlete/Profile/ProfileScreen'
import ProfileEditScreen from '../screens/Athlete/Profile/ProfileEditScreen'
import AccountSubscriptionScreen from '../screens/Athlete/Profile/AccountSubscriptionScreen'
import TabBarIcon from '../components/TabBarIcon'
import { navigationOptions, tabBarOptions } from '../shared/navigation'

const Stack = createStackNavigator()
const Tab = createBottomTabNavigator()

const VideosNavigator = () => (
  <Stack.Navigator screenOptions={navigationOptions}>
    <Stack.Screen name='AthleteVideosList' component={VideosListScreen} options={{ title: "Videos" }} />
    <Stack.Screen name='AthleteVideoDetail' component={VideoDetailScreen} options={{ title: "Video Details" }} />
    <Stack.Screen name='AthleteVideoFollowup' component={VideoFollowupScreen} options={{ title: "Ask a Followup" }} />
  </Stack.Navigator>
)

const NewVideoNavigator = () => (
  <Stack.Navigator screenOptions={navigationOptions}>
    <Stack.Screen name='AthleteNewVideo' component={NewVideoScreen} options={{ title: "New Video" }} />
  </Stack.Navigator>
)

const NewVideoModalNavigator = () => (
  <Stack.Navigator screenOptions={navigationOptions}>
    <Stack.Screen name='AthleteNewVideoRecord' component={NewVideoRecordScreen} options={{ title: null, headerShown: false }} />
    <Stack.Screen name='AthleteNewVideoPicker' component={NewVideoPickerScreen} options={{ title: null, headerShown: false }} />
    <Stack.Screen name='AthleteNewVideoConfirm' component={NewVideoConfirmScreen} options={{ title: "Confirm" }} />
  </Stack.Navigator>  
)

const ProfileNavigator = () => (
  <Stack.Navigator screenOptions={navigationOptions}>
    <Stack.Screen name='AthleteProfile' component={ProfileScreen} options={{ title: "Profile" }} />
    <Stack.Screen name='AthleteProfileEdit' component={ProfileEditScreen} options={{ title: "Edit Your Profile" }} />
    <Stack.Screen name='AccountSubscription' component={AccountSubscriptionScreen} options={{ title: "Subscription" }} />
  </Stack.Navigator>
)

const AthleteMain = () => (
  <Tab.Navigator
    tabBarOptions={tabBarOptions}
    >
    <Tab.Screen
      name="Videos"
      component={VideosNavigator}
      options={{tabBarIcon: ({focused}) => (
        <TabBarIcon icon="videos" focused={focused} />
      )}} />
    <Tab.Screen
      name="New Video"
      component={NewVideoNavigator}
      options={{tabBarIcon: ({focused}) => (
        <TabBarIcon icon="new-video" focused={focused} />
      )}} />
    <Tab.Screen
      name="Profile"
      component={ProfileNavigator}
      options={{tabBarIcon: ({focused}) => (
        <TabBarIcon icon="profile" focused={focused} />
      )}} />
  </Tab.Navigator>
)

const AthleteNavigator = (
  <Stack.Navigator mode="modal" screenOptions={navigationOptions} headerMode="none">
    <Stack.Screen name="AthleteMain" component={AthleteMain} />
    <Stack.Screen name="AthleteNewVideoModal" component={NewVideoModalNavigator} options={{ title: null }} />
  </Stack.Navigator>
)

export default AthleteNavigator
