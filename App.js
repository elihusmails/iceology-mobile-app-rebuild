import React from 'react'
import Purchases from 'react-native-purchases'
import * as Sentry from '@sentry/react-native';

import Providers from './navigation/Providers.js'

import { REVENUECAT_API_KEY, SENTRY_DSN } from './shared/constants'

Sentry.init({ 
  dsn: SENTRY_DSN,
  //debug: true,
  //environment: 'production' // Change this via .env variable?
});

// Purchases.setDebugLogsEnabled(true) // Only enable this for development
Purchases.setup(REVENUECAT_API_KEY) // Link Purchases with the user on login state change, not upfront

export default function App() {
  return <Providers />;
}