import React from 'react';
import {ImageBackground, Platform, ScrollView, View} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import {
  darkBlue,
  contentPadding,
} from '../shared/constants';

const ContentWrapper = ({ children, style, type = '' }) => {  
  const containerStyle = {...defaultStyle, ...style}
  
  const view = (
    <View style={containerStyle}>
      {children}
    </View>
  )
  
  const scrollView = (
    <ScrollView
      //contentContainerStyle={{ flex: 1 }}
    >
      <View style={containerStyle}>
        {children}
      </View>
    </ScrollView>
  )
  
  const keyboardAwareScrollView = (
    <KeyboardAwareScrollView
      //automaticallyAdjustContentInsets={false}
      //contentContainerStyle={{ flex: 1 }}
    >
      <View style={containerStyle}>
        {children}
      </View>
    </KeyboardAwareScrollView>    
  )
    
  const viewTypes = Platform.select({
    ios: {
      'View': view,
      'ScrollView': scrollView,
      'KeyboardAwareScrollView': keyboardAwareScrollView,
    },
    android: {
      'View': view,
      'ScrollView': scrollView,
      'KeyboardAwareScrollView': scrollView, // Android supports keyboard-aware scrolling natively
    }
  });
  
  return (
    <ImageBackground
      source={require('../assets/images/default-background.png')}
      style={imageBackgroundStyle}
    >
      {viewTypes[type]}
    </ImageBackground>
  );
}

const imageBackgroundStyle = {
  width: '100%',
  height: '100%',
  backgroundColor: darkBlue,
}

const defaultStyle = {
  paddingTop: contentPadding,
  paddingBottom: contentPadding,
  paddingLeft: contentPadding,
  paddingRight: contentPadding,
  minHeight: '100%'
}

export default ContentWrapper
