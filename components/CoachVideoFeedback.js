import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import firebase from '@react-native-firebase/app';
import '@react-native-firebase/firestore';

import Timeline from 'react-native-timeline-feed';
import moment from 'moment';
import styled, {css} from '@emotion/native';

import {font} from '../shared/utilities';
import {darkBlue, textColor} from '../shared/constants';
import {SmallText} from '../shared/typography';

import PrimaryButton from './Buttons';

const TimelineItemTimestamp = styled.Text({
  color: darkBlue,
  ...font({
    size: 11,
    weight: 'Bold',
  }),
  textTransform: 'uppercase',
  letterSpacing: 0.2,
  padding: 0,
  lineHeight: 16,
});

const StatusText = styled.Text({
  color: textColor,
  ...font({
    size: 11,
    weight: 'Bold',
  }),
  textTransform: 'uppercase',
  letterSpacing: 0.2,
  padding: 0,
  lineHeight: 16,
});

const messageTypes = {
  USER_FEEDBACK_REQUEST: 'Athlete requested feedback',
  USER_FOLLOWUP_REQUEST: 'Athlete requested a follow-up',
  COACH_FEEDBACK: 'Pro provided feedback',
  COACH_FOLLOWUP: 'Pro provided a follow-up',
};
class CoachVideoFeedback extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      messages: [],
    };

    this.sendMessage = this.sendMessage.bind(this);
    this.handleGiveFeedbackPress = this.handleGiveFeedbackPress.bind(this);
    this.handleGiveFollowupPress = this.handleGiveFollowupPress.bind(this);
    //this.renderTimelineItem = this.renderTimelineItem.bind(this);
  }

  componentDidMount() {
    const video = this.props.video;

    this.messagesRef = firebase
      .firestore()
      .collection('videos')
      .doc(video.id)
      .collection('messages');

    this.messagesUnsubscribe = this.messagesRef
      .orderBy('createdAt', 'desc')
      .onSnapshot((querySnapshot) => {
        let messages = [];
        querySnapshot.forEach((doc) => {
          messages.push({_id: doc.id, ...doc.data()});
        });

        this.setState({messages: messages});
      });
  }

  componentWillUnmount() {
    this.messagesUnsubscribe();
  }

  sendMessage(messages) {
    messages.forEach((message) => {
      const {text, user} = message;

      const data = {
        text,
        user,
        createdAt: firebase.firestore.FieldValue.serverTimestamp(),
      };

      const newMessageRef = this.messagesRef.doc();
      newMessageRef.set(data);
    });
  }

  handleGiveFeedbackPress() {
    this.props.navigation.navigate('CoachVideoFeedback', {
      video: this.props.video,
    });
  }

  handleGiveFollowupPress() {
    this.props.navigation.navigate('CoachVideoFollowup', {
      video: this.props.video,
    });
  }

  renderInteractionUI(message) {
    if (
      message.type === 'USER_FEEDBACK_REQUEST' &&
      this.props.video.status === 'AWAITING_FEEDBACK'
    ) {
      return (
        <PrimaryButton
          onPress={this.handleGiveFeedbackPress}
          text={'Reply to Request…'}
        />
      );
    }

    if (
      message.type === 'USER_FOLLOWUP_REQUEST' &&
      this.props.video.status === 'AWAITING_FOLLOWUP'
    ) {
      return (
        <PrimaryButton
          onPress={this.handleGiveFollowupPress}
          text={'Reply to Request…'}
        />
      );
    }
  }

  renderTimelineItem = ({item, index, isLast, otherProps}) => {
    return (
      <Timeline.Row>
        <Timeline.VerticalSeparator style={{}}>
          <Timeline.Circle color={'#ffffff66'}>
            <Timeline.Dot color={'#ffffff'} />
          </Timeline.Circle>
          {!isLast && <Timeline.Line color={'#ffffff66'} />}
        </Timeline.VerticalSeparator>
        <Timeline.Event style={styles.event}>
          <View style={styles.titleAndTimeContainer}>
            <Timeline.Title>
              <StatusText>{item.title}</StatusText>
            </Timeline.Title>
            <TimelineItemTimestamp style={{marginLeft: 4}}>
              <StatusText style={{fontWeight: '300'}}>{item.time}</StatusText>
            </TimelineItemTimestamp>
          </View>
          <View>
            <SmallText>{item.description}</SmallText>
            {item.interactionUI}
          </View>
        </Timeline.Event>
      </Timeline.Row>
    );
  };

  render() {
    //const currentUserUid = firebaseCurrentUser().uid;

    const timelineData = this.state.messages
      .sort((a, b) => a.createdAt > b.createdAt)
      .map((message) => {
        const labelTimestamp =
          message.createdAt && moment.unix(message.createdAt.seconds).fromNow();

        return {
          key: message._id,
          time: labelTimestamp,
          title: messageTypes[message.type], //labelText,
          description: `“${message.text}”`,
          lineColor: '#ffffff99',
          circleColor: '#ffffff66',
          interactionUI: this.renderInteractionUI(message),
        };
      });

    return (
      <Timeline
        data={timelineData}
        endWithCircle
        renderItem={this.renderTimelineItem}
        //horizontal={true}
      />
    );
  }
}

const styles = StyleSheet.create({
  titleAndTimeContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginBottom: 4,
  },
  time: {
    color: '#ffffff',
  },
  event: {
    marginTop: 0,
    paddingBottom: 20,
    paddingHorizontal: 8,
  },
  title: {
    color: '#ffffff',
  },
  description: {
    color: '#ffffff',
  },
});

export default CoachVideoFeedback;
