import React, { useState } from 'react'
import _ from 'lodash'
import { compose } from 'recompose'
import { Picker } from 'react-native-ui-lib'
import { withFormikControl } from 'react-native-formik'

import { textColor, errorColor } from '../shared/constants'

const FormPickerInput = ({ props, label, options, value, error, setFieldValue, ...otherProps }) => {
  const [ pickerValue, setPickerValue ] = useState(value);
  
  // Schroedinger's value; the Picker doesn't seem to update with the current value
  // unless I console.log() it. Uhhhhhh?
  //console.log({pickerValue})
  
  return (
    <Picker
      enableErrors={true}
      error={error}
      errorColor={errorColor}
      placeholder={label}
      placeholderTextColor={'rgba(255, 255, 255, 0.5)'}
      floatingPlaceholder={true}
      floatingPlaceholderColor={{
        default: 'rgba(255, 255, 255, 0.75)', error: errorColor, focus: 'rgba(255, 255, 255, 0.75)', disabled: '#FFFFFF'
      }}
      floatingPlaceholderStyle={{
        fontFamily: 'Gilroy'
      }}
      underlineColor={{
        default: 'rgba(255, 255, 255, 0.25)', error: errorColor, focus: '#FFFFFF', disabled: '#CCCCCC'
      }}
      useNativePicker
      floatOnFocus={true}
      value={pickerValue}
      style={{
        color: textColor,
      }}
      onChange={(value) => {
        setPickerValue(value);
        setFieldValue(value);
      }}
    >
      {_.map(options, option => (
        <Picker.Item key={option.value} value={option.value} label={option.label} disabled={option.disabled}/>
      ))}
    </Picker>
  )
}

export default withFormikControl(FormPickerInput)