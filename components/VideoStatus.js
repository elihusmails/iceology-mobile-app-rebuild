import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { Badge } from "native-base";
import Icon from 'react-native-vector-icons/AntDesign';
import styled, { css } from "@emotion/native";

import { green, orange, darkBlue, textColor } from "../shared/constants";
import { font } from "../shared/utilities";

const statuses = {
  AWAITING_FEEDBACK: {
    text: "Waiting for pro feedback",
    color: textColor + "99",
    icon: {
      type: "AntDesign",
      name: "clockcircle"
      //color: orange
    }
  },
  RECEIVED_FEEDBACK: {
    text: "Pro feedback received",
    color: textColor,
    icon: {
      type: "AntDesign",
      name: "exclamationcircle"
      //color: green
    }
  },
  AWAITING_FOLLOWUP: {
    text: "Waiting for pro follow-up",
    color: textColor + "99",
    icon: {
      type: "AntDesign",
      name: "clockcircle"
      //color: orange
    }
  },
  COMPLETE: {
    text: "Pro feedback complete",
    color: textColor,
    icon: {
      type: "AntDesign",
      name: "checkcircle"
      //color: green
    }
  }
};

const StyledText = styled.Text(props => ({
  color: props.color || textColor,
  ...font({
    size: 11,
    weight: "Bold"
  }),
  textTransform: "uppercase",
  letterSpacing: 0.2,
  padding: 0,
  lineHeight: 16
}));

class VideoStatus extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const statusText = statuses[this.props.status].text;
    const statusColor = statuses[this.props.status].color;
    const statusIcon = statuses[this.props.status].icon;
    const statusIconColor =
      statuses[this.props.status].icon.color || statusColor;

    return (
      <View
        style={{
          flex: 1,
          flexDirection: "row",
          alignItems: "center"
        }}
      >
        <Icon
          name={statusIcon.name}
          type={statusIcon.type}
          color={statusIconColor}
          style={{
            fontSize: 12,
            color: statusIconColor,
            lineHeight: 0,
            marginRight: 4
          }}
        />
        <StyledText color={statusColor}>{statusText}</StyledText>
      </View>
    );
  }
}

export default VideoStatus;
