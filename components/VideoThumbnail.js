/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Image, View} from 'react-native';
import styled from '@emotion/native';

import {darkBlue} from '../shared/constants';

const ThumbnailBorder = styled.View({
  borderWidth: 0,
  padding: 3,
  backgroundColor: '#ffffff80',
  borderRadius: 8,
  marginRight: 10,
});

class VideoThumbnail extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const video = this.props.video;
    //console.log({video})

    return (
      <View>
        <ThumbnailBorder>
          {video.thumbnail && video.thumbnail.downloadURL ? (
            <Image
              style={{
                width: 76,
                height: 76,
              }}
              borderRadius={4}
              source={{uri: video.thumbnail.downloadURL}}
            />
          ) : (
            <View
              style={{
                width: 76,
                height: 76,
                backgroundColor: darkBlue,
              }}
              borderRadius={4}
            />
          )}
        </ThumbnailBorder>
      </View>
    );
  }
}

export default VideoThumbnail;
