import React from 'react'
import {FlatList, Image, View} from 'react-native'
import styled, {css} from '@emotion/native'
import moment from 'moment'

import {font} from '../shared/utilities'
import {textColor} from '../shared/constants'

import VideoStatus from '../components/VideoStatus'

const TouchableArea = styled.TouchableOpacity({
  padding: 0,
})

const VideoLayout = styled.View({
  flex: 1,
  flexDirection: 'row',
  justifyContent: 'flex-start',
  margin: 16,
})

const VideoTitle = styled.Text({
  ...font({
    size: 20,
    weight: 'Bold',
  }),
  color: '#ffffff',
  marginTop: 4,
  marginBottom: 0,
})

const VideoTimestamp = styled.Text({
  marginBottom: 0,
  color: textColor,
  ...font({weight: 'Regular', size: 14}),
})

const ThumbnailBorder = styled.View({
  borderWidth: 0,
  padding: 3,
  backgroundColor: '#ffffff80',
  borderRadius: 8,
  marginRight: 12,
})

const VideosList = ({ videos, handleVideoPress }) => {
  return (
    <FlatList
      style={{paddingHorizontal: 8, paddingVertical: 12}}
      data={videos}
      keyExtractor={(video, index) => video.id}
      renderItem={({item: video, separators}) => {
        //console.log(video)
        
        return (
          <TouchableArea onPress={() => handleVideoPress(video)}>
            <VideoLayout>
              <View>
                <ThumbnailBorder>
                  {video.thumbnail ? (
                    <Image
                      style={{
                        width: 76,
                        height: 76,
                      }}
                      borderRadius={4}
                      source={{uri: video.thumbnail.downloadURL}}
                    />
                  ) : (
                    <View
                      style={{
                        width: 76,
                        height: 76,
                        backgroundColor: 'black',
                      }}
                      borderRadius={4}
                    />
                  )}
                </ThumbnailBorder>
              </View>

              <View>
                <VideoTitle>{video.title}</VideoTitle>
                <VideoTimestamp>
                  Submitted{' '}
                  {video.createdAt &&
                    moment.unix(video.createdAt.seconds).fromNow()}
                </VideoTimestamp>
                <VideoStatus status={video.status} />
              </View>
            </VideoLayout>
          </TouchableArea>
        )
      }}
    />
  )
}

export default VideosList
