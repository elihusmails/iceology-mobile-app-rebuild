import React, { useContext } from 'react'
import { Alert, Button } from 'react-native'

import { AuthContext } from '../navigation/AuthProvider'

const OnBoardingCancelButton = () => {
  const { logout } = useContext(AuthContext)

  return (
    <Button
      title="Quit"
      onPress={() => {
        Alert.alert(
          'Quit and log out of Iceology?',
          'You may log in and continue anytime using the email address and password you already created',
          [
            {
              text: 'Quit',
              onPress: async () => {
                try {
                  logout()
                } catch (error) {
                  console.error(error)
                }
              },
              style: 'destructive'
            },
            {
              text: 'Continue',
              onPress: () => {},
              style: 'cancel',
            },
          ],
          {cancelable: false},
        )
      }}
    />    
  )
}

export default OnBoardingCancelButton