import React from "react"
import { TextField } from "rn-material-ui-textfield"

import { textColor } from "../shared/constants"

class FormTextField extends React.PureComponent {
  constructor(props) {
    super(props)
  }

  focus(props) {
    this._textInput.focus()
  }

  render() {
    //const { error, value, onChangeText, onBlur, label } = this.props

    return (
      <TextField
        ref={component => this._textInput = component}
        value={this.props.value}
        //onChangeText={setFieldValue}
        onChangeText={this.props.onChangeText}
        onBlur={this.props.onBlur}
        textColor={textColor}
        baseColor={'rgba(255, 255, 255, 0.6)'}
        tintColor={'rgba(255, 255, 255, 1.0)'}
        lineWidth={1} activeLineWidth={1}
        labelFontSize={14}
        labelTextStyle={{
          fontFamily: 'Gilroy',
          fontWeight: '500'
        }}
        {...this.props}
      />
    )
  }
}

export default FormTextField
