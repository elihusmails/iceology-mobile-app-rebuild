import React, { Component } from "react";
import { Modal as NativeModal } from "react-native";
import styled, { css } from "@emotion/native";

import { contentPadding } from "../shared/constants";

const Container = styled.View({
  padding: contentPadding,
  backgroundColor: "#000000cc",
  flex: 1,
  top: 0,
  bottom: 0,
  justifyContent: "center",
  alignItems: "center"
});

class Modal extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <NativeModal
        animationType="fade"
        transparent={true}
        visible={this.props.visible}
      >
        <Container>{this.props.children}</Container>
      </NativeModal>
    );
  }
}

export default Modal;
