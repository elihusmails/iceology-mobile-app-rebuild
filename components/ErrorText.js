import React from 'react'
import { StyleSheet, Text } from 'react-native'
import { errorColor } from '../shared/constants'

export default function ErrorText(error) {
  let errorText = null
  
  if (typeof error === 'string') {
    errorText = error
  }
  
  if (typeof error === 'object' && typeof error.error === 'string') {
    errorText = error.error
  }
  
  if (errorText !== null) {
    return (
      <Text style={styles.errorText}>
        ERROR: {errorText}
      </Text>
    )
  } else {
    return <></>
  }
}

const styles = StyleSheet.create({
  errorText: {
    color: errorColor,
    fontWeight: 'bold',
    marginBottom: 8
  }
})
