import React, { Component } from "react";
import { View } from "react-native";

class Center extends Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: "column",
          alignItems: "center"
        }}
      >
        {this.props.children}
      </View>
    );
  }
}

export default Center;
