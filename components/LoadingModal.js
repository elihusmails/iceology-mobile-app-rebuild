import React, { Component } from "react";
import { ActivityIndicator, Modal, Platform } from "react-native";
import styled, { css } from "@emotion/native";

import { contentPadding } from "../shared/constants";

const LayoutContainer = styled.View({
  padding: contentPadding,
  backgroundColor: "#000000cc",
  flex: 1,
  top: 0,
  bottom: 0,
  justifyContent: "center",
  alignItems: "center"
});

class LoadingModal extends React.Component {
  state = {
    visible: false
  };

  hide({ then } = {}) {
    if (Platform.OS === 'ios') {
      this.setState({ visible: false, onDismiss: then });
    } else {
      this.setState({ visible: false });
      if (then !== undefined) {
        then();
      }
    }
  }

  show() {
    this.setState({ visible: true });
  }

  render() {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.visible}
        onDismiss={this.state.onDismiss}
        {...this.props}
      >
        <LayoutContainer>
          <ActivityIndicator size="large" color="#ffffff" />
        </LayoutContainer>
      </Modal>
    );
  }
}

export default LoadingModal;
