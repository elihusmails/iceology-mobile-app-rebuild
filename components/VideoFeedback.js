import React, {useEffect, useRef, useState} from 'react';
import {Alert, StyleSheet, View} from 'react-native';

import firebase from '@react-native-firebase/app';
import '@react-native-firebase/firestore';

import Timeline from 'react-native-timeline-feed';
import moment from 'moment';
import styled from '@emotion/native';

import {darkBlue} from '../shared/constants';
import {font} from '../shared/utilities';
import {H3, SmallText, StatusText} from '../shared/typography';

import Spacer from '../components/Spacer';
import PrimaryButton from '../components/Buttons';

const TimelineItemTimestamp = styled.Text({
  color: darkBlue,
  ...font({
    size: 11,
    weight: 'Bold',
  }),
  textTransform: 'uppercase',
  letterSpacing: 0.2,
  padding: 0,
  lineHeight: 16,
})

const messageTypes = {
  USER_FEEDBACK_REQUEST: 'You requested feedback',
  USER_FOLLOWUP_REQUEST: 'You requested a follow-up',
  COACH_FEEDBACK: 'Pro provided feedback',
  COACH_FOLLOWUP: 'Pro provided a follow-up',
}

const VideoFeedback = ({ navigation, route, video }) => {
  //const { video } = route.params
  const messagesRef = useRef()
  const [messages, setMessages] = useState([])
  const [loading, setLoading] = useState(false)

  useEffect(
    () => {      
      messagesRef.current =
        firebase
          .firestore()
          .collection('videos')
          .doc(video.id)
          .collection('messages')
      
      const unsubscribe = messagesRef.current
        .orderBy('createdAt', 'desc')
        .onSnapshot(snapshot => {
          let messages = []
          
          snapshot.forEach((doc) => {
            messages.push({id: doc.id, ...doc.data()})
          })

          setLoading(false)          
          setMessages(messages)
        },
        error => { console.log({error}) }
      )

      return () => unsubscribe()
    },
    []
  )

  const sendMessage = (messages) => {
    messages.forEach((message) => {
      const {text, user} = message
      const data = {
        text,
        user,
        createdAt: firebase.firestore.FieldValue.serverTimestamp(),
      }

      const newMessageRef = messagesRef.current.doc()
      newMessageRef.set(data)
    })
  }

  const handleAcceptFeedbackPress = () => {
    const videoRef = firebase.firestore().collection('videos').doc(video.id)

    videoRef.update({
      status: 'COMPLETE',
    })

    Alert.alert(
      'Thanks!',
      'Thanks for your question! Your feedback for this video is complete.',
      [
        {
          text: 'OK',
          onPress: () => {
            navigation.navigate('AthleteVideosList');
          },
        },
      ],
      {cancelable: false},
    )
  }

  const handleRejectFeedbackPress = () => {
    navigation.navigate('AthleteVideoFollowup', {
      video: video,
    });
  }

  const renderInteractionUI = (message) => {
    if (video.status === 'COMPLETE') {
      return
    }

    if (
      !(
        message.type === 'COACH_FEEDBACK' &&
        video.status === 'RECEIVED_FEEDBACK'
      )
    ) {
      return
    }

    return (
      <View>
        <H3>Did this answer your question?</H3>
        <Spacer height={8} />
        <PrimaryButton
          onPress={handleAcceptFeedbackPress}
          text={'Yes, thanks for the feedback!'}
        />
        <Spacer height={8} />
        <PrimaryButton
          onPress={handleRejectFeedbackPress}
          text={'No, ask a follow-up'}
        />
      </View>
    );
  }

  const renderTimelineItem = ({item, index, isLast, otherProps}) => {
    const isPro = item.user.role === 'coach'

    return (
      <Timeline.Row>
        <Timeline.VerticalSeparator style={{}}>
          <Timeline.Circle color={'#ffffff66'}>
            <Timeline.Dot color={'#ffffff'} />
          </Timeline.Circle>
          {!isLast && <Timeline.Line color={'#ffffff66'} />}
        </Timeline.VerticalSeparator>
        <Timeline.Event style={styles.event}>
          <View style={styles.titleAndTimeContainer}>
            <Timeline.Title>
              <StatusText>{item.title}</StatusText>
            </Timeline.Title>
            <TimelineItemTimestamp style={{marginLeft: 4}}>
              <StatusText style={{fontWeight: '300'}}>{item.time}</StatusText>
            </TimelineItemTimestamp>
          </View>
          <View>
            <SmallText style={{fontWeight: isPro ? '700' : '300'}}>
              {item.description}
            </SmallText>
            {item.interactionUI}
          </View>
        </Timeline.Event>
      </Timeline.Row>
    )
  }

  const timelineData = messages
    .sort((a, b) => a.createdAt > b.createdAt)
    .map((message) => {
      const labelTimestamp =
        message.createdAt && moment.unix(message.createdAt.seconds).fromNow();

      return {
        key: message._id,
        user: message.user,
        time: labelTimestamp,
        title: messageTypes[message.type], //labelText,
        description: `“${message.text}”`,
        lineColor: '#ffffff99',
        circleColor: '#ffffff66',
        interactionUI: renderInteractionUI(message),
      };
    });

  return (
    <Timeline
      data={timelineData}
      endWithCircle
      renderItem={renderTimelineItem}
    />
  )
}

const styles = StyleSheet.create({
  titleAndTimeContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginBottom: 4,
  },
  time: {
    color: '#ffffff',
  },
  event: {
    marginTop: 0,
    paddingBottom: 20,
    paddingHorizontal: 8,
  },
  title: {
    color: '#ffffff',
  },
  description: {
    color: '#ffffff',
  },
});

export default VideoFeedback;
