import React from "react";
import { View } from "react-native";
import { Formik } from "formik";
import { compose } from "recompose";
import {
  handleTextInput,
  withFormikControl,
  withNextInputAutoFocusForm,
  withNextInputAutoFocusInput
} from "react-native-formik";
import * as Yup from "yup";
import FormTextField from "./FormTextField";

import Spacer from "./Spacer";
import PrimaryButton from "./Buttons";

const FormikTextField = withFormikControl(compose(
  handleTextInput,
  withNextInputAutoFocusInput
)(FormTextField));
const FormikInputsContainer = withNextInputAutoFocusForm(View, { submitAfterLastInput: false });

const validationSchema = Yup.object().shape({
  email: Yup.string()
    .required()
    .email("That’s not an email address"),
  password: Yup.string()
    .required()
    .min(6, "Password is too short")
});

export default props => (
  <Formik
    onSubmit={values => props.onSubmit(values)}
    validationSchema={validationSchema}
  >
    {props => {
      return (
        <FormikInputsContainer>
          <FormikTextField name="email" label="Email address" type="email" />
          <FormikTextField name="password" label="Create a password" type="password" />

          <Spacer height={18} />

          <PrimaryButton onPress={props.handleSubmit} text="Register" />
        </FormikInputsContainer>
      );
    }}
  </Formik>
);