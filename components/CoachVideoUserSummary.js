import React from "react";
import { View } from "react-native";
import styled, { css } from "@emotion/native";

import { firebaseGetUser } from "../shared/firebase";
import { BodyText, LabelText } from "../shared/typography";

const TouchableArea = styled.TouchableOpacity({
  padding: 0
});

const Row = styled.View({
  flex: 1,
  flexDirection: "row",
  //justifyContent: "space-between"
});

const Cell = styled.View({
  marginRight: 8
});

class CoachVideoUserSummary extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      user: null
    }
  }

  static navigationOptions = ({ navigation }) => {
    return {
      title: "Video Details"
    };
  };

  async componentDidMount() {
    const user = await firebaseGetUser(this.props.uid);
    this.setState({ user: user })
  }

  handlePress() {
    this.props.navigation.navigate("CoachVideoUser", {
      userId: this.props.uid
    });
  }

  render() {
    const user = this.state.user;

    if (user === null) {
      return <View></View>;
    }

    return (
      <View>
        <TouchableArea
          onPress={() => this.handlePress()}>
          <Row>
            <Cell>
              <LabelText>Name</LabelText>
              <BodyText>{user.profile.name}</BodyText>
            </Cell>

            <Cell>
              <LabelText>Birth year</LabelText>
              <BodyText>{user.profile.birthYear}</BodyText>
            </Cell>
          </Row>
          <Row>
            <Cell>
              <LabelText>Gender</LabelText>
              <BodyText>{user.profile.gender}</BodyText>
            </Cell>

            <Cell>
              <LabelText>Height</LabelText>
              <BodyText>{user.profile.height && this.inchesToHeight(user.profile.height)}</BodyText>
            </Cell>

            <Cell>
              <LabelText>Weight</LabelText>
              <BodyText>{user.profile.weight} lbs.</BodyText>
            </Cell>
          </Row>
        </TouchableArea>
      </View>
    )
  }

  inchesToHeight(heightInInches) {
    const feet = Math.floor(heightInInches / 12);
    const inches = heightInInches % 12;
    return `${feet}' ${inches}"`;
  }
}

export default CoachVideoUserSummary;
