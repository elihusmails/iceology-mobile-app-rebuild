import React, { Component } from "react";
//import { Text, View } from "react-native";
import styled, { css } from "@emotion/native";

import { font } from "../shared/utilities";
import { highlightColor, textColor } from "../shared/constants";

const ButtonContainer = styled.TouchableOpacity({
  padding: 8,
  backgroundColor: highlightColor,
  borderRadius: 8,
  borderWidth: 3,
  borderColor: "#ffffff75"
});

const ButtonContainerDisabled = styled.View({
  padding: 8,
  backgroundColor: highlightColor,
  borderRadius: 8,
  borderWidth: 3,
  borderColor: "#ffffff75",
  opacity: 0.5
});

const ButtonText = styled.Text({
  ...font({
    family: "Gilroy",
    weight: "Medium",
    size: 16
  }),
  color: textColor,
  textAlign: "center"
});

class PrimaryButton extends Component {
  render() {
    const { onPress, disabled } = this.props;
    const text = this.props.text.toUpperCase();
    const buttonText = <ButtonText>{text}</ButtonText>;

    if (!disabled) {
      return (
        <ButtonContainer onPress={() => onPress()}>
          {buttonText}
        </ButtonContainer>
      );
    } else {
      return <ButtonContainerDisabled>{buttonText}</ButtonContainerDisabled>;
    }
  }
}

export default PrimaryButton;
