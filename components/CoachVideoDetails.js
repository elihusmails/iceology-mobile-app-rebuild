/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View} from 'react-native';
import moment from 'moment';
import styled from '@emotion/native';

import {contentPadding} from '../shared/constants';
import {H3, TimestampText} from '../shared/typography';

import VideoStatus from '../components/VideoStatus';
//import VideoThumbnail from '../components/VideoThumbnail';
import VideoPreview from '../components/VideoPreview';

const Layout = styled.View({
  flex: 1,
  flexDirection: 'row',
  justifyContent: 'flex-start',
  marginBottom: contentPadding,
});

class CoachVideoDetails extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const video = this.props.video;

    return (
      <View>
        <Layout>
          <VideoPreview video={video} navigation={this.props.navigation} />

          <View>
            <H3>{video.title}</H3>
            <TimestampText>
              {video.createdAt &&
                moment.unix(video.createdAt.seconds).fromNow()}
            </TimestampText>
            <VideoStatus status={video.status} />
          </View>
        </Layout>
      </View>
    );
  }
}

export default CoachVideoDetails;
