import React, { useState, useContext } from "react"
import { Text, Keyboard, View } from "react-native"
import * as Yup from "yup"

import { AuthContext } from "../navigation/AuthProvider"
import Form from "./Form"
import FormTextInput from "./FormTextInput"
import FormError from "./FormError"
import Spacer from "./Spacer"
import PrimaryButton from "./Buttons"

const initialValues = { email: '' }

const validationSchema = Yup.object().shape({
  email: Yup.string().required()
})

const ForgotPasswordForm = ({ onSubmit }) => {
  const { error } = useContext(AuthContext)
  
  return (
    <Form
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={values => onSubmit(values)}
    >
      {({ handleSubmit, errors }) => (
        <View>
          <FormError error={error} />

          <FormTextInput
            name="email"
            label="Email address"
            error={errors.email}
            autoCapitalize="none"
            keyboardType="email-address"
            autoCorrect={false}
            type="email" />

          <Spacer height={18} />
          
          <PrimaryButton onPress={handleSubmit} text="Send password reset email" />
        </View>
      )}      
    </Form>
  )
}

export default ForgotPasswordForm
