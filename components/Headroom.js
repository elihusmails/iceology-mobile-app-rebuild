import React from "react";
import { View } from "react-native";
import { navigationHeadroom } from "../shared/constants";

class Headroom extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return <View style={{ height: navigationHeadroom }} />;
  }
}

export default Headroom;
