import React, { useContext, useEffect, useLayoutEffect, useState } from 'react'
import { Alert, Button, View } from 'react-native'
import moment from 'moment'
import Purchases from 'react-native-purchases'

import { AuthContext } from '../../navigation/AuthProvider'
import { getAvailablePackages } from '../../shared/purchases'
import { TextButton } from '../../shared/typography'
import LoadingModal from '../LoadingModal'

const RestorePurchasesForm = ({ navigation }) => {
  const [submitting, setSubmitting] = useState(null)

  const handleRestorePurchases = async () => {
    setSubmitting(true)
    
    try {
      const restoredPurchaserInfo = await Purchases.restoreTransactions()
      //console.log({ restoredPurchaserInfo })
      const activeSubscriptions = restoredPurchaserInfo.activeSubscriptions
      if (activeSubscriptions && activeSubscriptions.length > 0) {
        Alert.alert('Success!', 'Your subscription has been restored');      
      } else {
        Alert.alert('Error', 'No subscriptions were found to restore');
      }
      setSubmitting(false)
    } catch (e) {
      setSubmitting(false)
      if (!e.userCancelled) {
        Alert.alert('Error', e)
      }
    }
  }
    
  return (
    <View>
      <TextButton onPress={handleRestorePurchases}>
        Restore Purchases
      </TextButton>

      <LoadingModal visible={submitting} />
    </View>
  )
}

export default RestorePurchasesForm