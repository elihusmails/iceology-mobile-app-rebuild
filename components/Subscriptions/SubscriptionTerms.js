/* eslint-disable react-native/no-inline-styles */
import React from 'react'
import {Linking, Platform, View} from 'react-native'

import {SmallText} from '../../shared/typography'
import {contentPadding, darkBlue} from '../../shared/constants'

class SubscriptionTerms extends React.Component {
  render() {
    const terms = Platform.select({
      ios: (
        <View>
          <SmallText>
            Payment will be charged to iTunes Account at confirmation of
            purchase.
          </SmallText>
          <SmallText>
            Subscription automatically renews unless auto-renew is turned off at
            least 24-hours before the end of the current period.
          </SmallText>
          <SmallText>
            Account will be charged for renewal within 24-hours prior to the end
            of the current period, and identify the cost of the renewal.
          </SmallText>
          <SmallText>
            Subscriptions may be managed by the user and auto-renewal may be
            turned off by going to the user's Account Settings after purchase.
          </SmallText>
          <SmallText>
            Any unused portion of a free trial period, if offered, will be
            forfeited when the user purchases a subscription to that
            publication, where applicable￼.
          </SmallText>
        </View>
      ),
      android: <View />,
    });

    return (
      <View
        style={{
          backgroundColor: darkBlue + '99',
          padding: contentPadding,
          borderRadius: 8,
        }}>
        {terms}

        <SmallText
          style={{fontWeight: 'bold'}}
          onPress={() =>
            this.openUrl('https://iceology-app.firebaseapp.com/privacy/')
          }>
          Privacy Policy
        </SmallText>
        <SmallText
          style={{fontWeight: 'bold'}}
          onPress={() =>
            this.openUrl('https://iceology-app.firebaseapp.com/terms/')
          }>
          Terms of Use
        </SmallText>
      </View>
    )
  }

  openUrl = url => {
    Linking.openURL(url)
  }
}

export default SubscriptionTerms
