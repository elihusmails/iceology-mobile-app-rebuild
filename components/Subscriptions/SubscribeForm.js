import React, { useContext, useEffect, useLayoutEffect, useState } from 'react'
import { Alert, Button, View } from 'react-native'
import Purchases from 'react-native-purchases'

import { getAvailablePackages } from '../../shared/purchases'

import {
  BodyText, SmallText
} from '../../shared/typography'

import Spacer from '../Spacer.js'
import LoadingModal from '../LoadingModal'
import PrimaryButton from '../Buttons'

const SubscribeForm = ({ navigation }) => {
  const [purchases, setPurchases] = useState(null)
  const [purchaserInfo, setPurchaserInfo] = useState(null)
  const [submitting, setSubmitting] = useState(null)
  const [error, setError] = useState(null)

  useEffect(() => {
    if (purchases === null) {
      initPurchases()
    }
  }, [])

  const initPurchases = async () => {
    try {
      const purchaser = await Purchases.getPurchaserInfo()
      setPurchaserInfo(purchaser)
      console.log(JSON.stringify(purchaser, null, 2))

      const availablePackages = await getAvailablePackages()
      if (availablePackages) {
        console.log({availablePackages})
        setPurchases(availablePackages)
      }
    } catch (error) {
      console.log({error})
    }
  }

  const handlePurchase = async (productPackage) => {
    setSubmitting(true)

    try {
      const { purchaserInfo, productIdentifier } = await Purchases.purchasePackage(productPackage)
      setPurchaserInfo(purchaserInfo)
      setSubmitting(false)
    } catch (e) {
      setSubmitting(false)
      if (!e.userCancelled) {
        Alert.alert('Error', e)
      }
    }
  }

  const isSubscribed = () => {
    if (!purchaserInfo) {
      return false
    }

    if (!purchaserInfo.activeSubscriptions) {
      return false
    }

    return purchaserInfo.activeSubscriptions.length > 0
  }

  const isSubscriptionActive = (productPackage) => {
    if (!purchaserInfo) {
      return false
    }

    if (!purchaserInfo.activeSubscriptions) {
      return false
    }

    return purchaserInfo.activeSubscriptions.indexOf(productPackage.product.identifier) !== -1
  }

  const renderPurchase = (productPackage) => {    
    const expires = purchaserInfo.allExpirationDates[productPackage.product.identifier]
    
    return (
      <View key={productPackage.product.identifier.replace('.', '-')}>
        <PrimaryButton
          onPress={() => handlePurchase(productPackage)}
          disabled={submitting || isSubscriptionActive(productPackage)}
          text={`Subscribe to ${productPackage.product.title}`}
        />

        <Spacer height={8} />

        {expires && ((new Date(expires)) > (Date.now())) && (
          <View>
            <SmallText
              style={{
                textAlign: 'center',
                fontWeight: 'bold',
                marginBottom: 0,
              }}>
              Expires {(new Date(expires)).toDateString()}
            </SmallText>
          </View>
        )}

        <BodyText style={{ fontSize: 14, textAlign: 'center' }}>
          {productPackage.product.description} – {productPackage.product.price_string}/month
        </BodyText>

        <Spacer height={24} />
      </View>
    )
  }

  return (
    <View>
      {purchases && Array.isArray(purchases) && purchases.map(
        productPackage => renderPurchase(productPackage)
      )}
      <LoadingModal visible={submitting} />
    </View>
  )
}

export default SubscribeForm