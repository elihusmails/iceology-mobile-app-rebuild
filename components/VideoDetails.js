import React from 'react';
import {View} from 'react-native';
import moment from 'moment';
import styled from '@emotion/native';

import {contentPadding} from '../shared/constants';
import {H3, TimestampText} from '../shared/typography';

import VideoStatus from '../components/VideoStatus';
import VideoPreview from '../components/VideoPreview';

const Layout = styled.View({
  flex: 1,
  flexDirection: 'row',
  justifyContent: 'flex-start',
  marginBottom: contentPadding,
});

const VideoDetails = ({ navigation, video }) => {
  return (
    <View>
      <Layout>
        <VideoPreview video={video} navigation={navigation} />

        <View>
          <H3>{video.title}</H3>
          <TimestampText>
            {video.createdAt &&
              moment.unix(video.createdAt.seconds).fromNow()}
          </TimestampText>
          <VideoStatus status={video.status} />
        </View>
      </Layout>
    </View>
  )
}

export default VideoDetails
