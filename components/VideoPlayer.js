import React from "react";
import { StyleSheet, Text, TouchableHighlight, View } from "react-native";
import Video from "react-native-video";

class VideoPlayer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      video: null
    };

    this.handlePlay = this.handlePlay.bind(this);
    this.didFocusSubscription = null;
  }

  componentDidMount() {
    this.didFocusSubscription = this.props.navigation.addListener(
      "didFocus",
      payload => {
        //console.debug('didBlur', payload);
        this.setState({
          video: this.props.video.downloadURL
        });
      }
    );
  }

  componentWillUnmount() {
    this.didFocusSubscription.remove();
  }

  onBuffer() {
    //console.error("buffering");
  }

  videoError() {
    //console.error("error");
  }

  handlePlay() {
    this.player.presentFullscreenPlayer();
  }

  render() {
    return (
      <View>
        {this.state.video && (
          <Video
            source={{ uri: this.state.video }}
            ref={ref => {
              this.player = ref;
            }}
            onBuffer={this.onBuffer}
            onError={this.videoError}
            paused={true}
            style={styles.backgroundVideo}
          />
        )}
        <TouchableHighlight onPress={this.handlePlay}>
          <Text>Play</Text>
        </TouchableHighlight>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  backgroundVideo: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    height: 200,
    borderRadius: 12
  }
});

export default VideoPlayer;

//<FlatList
//    ItemSeparatorComponent={Platform.OS !== 'android' && ({ highlighted }) => (
//        <View style={[style.separator, highlighted && { marginLeft: 0 }]} />
//    )}
//data = { [{ title: 'Title Text', key: 'item1' }]}
//renderItem = {({ item, separators }) => (
//    <TouchableHighlight
//        onPress={() => this._onPress(item)}
//        onShowUnderlay={separators.highlight}
//        onHideUnderlay={separators.unhighlight}>
//        <View style={{ backgroundColor: 'white' }}>
//            <Text>{item.title}</Text>
//        </View>
//    </TouchableHighlight>
//)}
///>
