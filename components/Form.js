import React from "react";
import { Formik } from "formik";

import { FormikInputsContainer } from "./FormikElements"

const Form = ({ children, props, initialValues, enableReinitialize, validationSchema, onSubmit }) => (
    <Formik
      initialValues={initialValues}
      enableReinitialize={enableReinitialize}
      onSubmit={values => onSubmit(values)}
      validationSchema={validationSchema}
    >
      {children}
    </Formik>    
  
)

export default Form