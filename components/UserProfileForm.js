import React from "react";
import { View } from "react-native";
import * as Yup from "yup"
import { Formik } from "formik";

import Spacer from "./Spacer";
import PrimaryButton from "./Buttons";
import Form from "./Form"
import FormTextInput from "./FormTextInput"
import FormPickerInput from "./FormPickerInput"
import { FormikInputsContainer } from "./FormikElements"

const validationSchema = Yup.object().shape({
  relationship: Yup.string().required(),
  name: Yup.string().required(),
  birthYear: Yup.string().required(),
  gender: Yup.string(),
  height: Yup.string().required(),
  weight: Yup.string().required(),
  playerNumber: Yup.string().required(),
  position: Yup.string().required(),
  shoots: Yup.string(),
  strengths: Yup.string(),
  weaknesses: Yup.string(),
  skills: Yup.string(),
  experience: Yup.string()
})

const UserProfileForm = ({ onSubmit, initialValues }) => {
  //console.log({initialValues})
  
  return (
    <Form
      initialValues={initialValues}
      enableReinitialize={true}
      validationSchema={validationSchema}
      onSubmit={values => onSubmit(values)}
    >
      {({ handleSubmit, values, errors }) => {
        //console.log(values)  
        return (
          <View>
            <FormPickerInput
              name="relationship"
              label="How are you using Iceology?"
              error={errors.relationship}
              //value={values.relationship}
              options={[
                { label: "", value: "" },
                { label: "I’m an athlete", value: "athlete" },
                { label: "I’m the parent of an athlete", value: "parent" }
              ]}
            />
            <FormTextInput name="name" label="Name" error={errors.name} autoCapitalize="words" autoCompleteType="name" />
            <FormTextInput name="birthYear" label="Birth year" error={errors.birthYear} keyboardType="numeric" maxLength={4} />
            <FormPickerInput
              name="gender"
              label="Gender"
              error={errors.gender}
              //value={values.gender}
              options={[
                { label: "", value: "" },
                { label: "Male", value: "male" },
                { label: "Female", value: "female" }
              ]}
            />
            <FormTextInput name="height" label="Height (inches)" error={errors.height} keyboardType="numeric" maxLength={2} />
            <FormTextInput name="weight" label="Weight (pounds)" error={errors.weight} keyboardType="numeric" maxLength={3} />
            <FormTextInput name="playerNumber" label="Player number" error={errors.playerNumber} keyboardType="numeric" />
            <FormTextInput name="position" label="Position" error={errors.position} autoCapitalize="words" />
            <FormPickerInput
              name="shoots"
              label="Shoots"
              error={errors.shoots}
              //value={values.shoots}
              options={[
                { label: "", value: "" },
                { label: "Left", value: "left" },
                { label: "Right", value: "right" },
                { label: "Both", value: "both" }
              ]}
            />
            <FormTextInput name="strengths" label="Strengths (optional)" error={errors.strengths} />
            <FormTextInput name="weaknesses" label="Weaknesses (optional)" error={errors.weaknesses} />
            <FormTextInput name="skills" label="Skills (optional)" error={errors.skills} />
            <FormTextInput name="experience" label="Experience (optional)" error={errors.experience} />
  
            <Spacer height={18} />
  
            <PrimaryButton onPress={handleSubmit} text="Save Profile" />
          </View>
        )
      }}
    </Form>
  )
}

export default UserProfileForm