import React from "react";
import { View } from "react-native";
import { Formik } from "formik";
import { compose } from "recompose";
import makeInput, {
  handleTextInput,
  withFormikControl,
  withNextInputAutoFocusForm,
  //withNextInputAutoFocusInput,
  //KeyboardModal,
  withPickerValues
} from "react-native-formik";

import FormTextInput from "./FormTextInput";

export const FormikInputsContainer = withNextInputAutoFocusForm(View, { submitAfterLastInput: false });

export const FormikTextField = withFormikControl(compose(
  handleTextInput,
  //withNextInputAutoFocusInput
)(FormTextInput));

export const FormikPicker = compose(
  makeInput,
  withPickerValues
)(FormTextInput);
