import React, { useRef, useState } from 'react'
import {
  //Dimensions,
  //Image,
  Platform,
  StyleSheet,
  //Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {Icon} from 'native-base';
import Video from 'react-native-video';

import VideoThumbnail from '../components/VideoThumbnail';

//const {windowHeight, windowWidth} = Dimensions.get('window');

const VideoPreview = ({ video, navigation }) => {
  const player = useRef(null)
  //const [video, setVideo] = useState(null)
  const [paused, setPaused] = useState(true)
  
  //console.log({video})
  
  //useEffect(() => {
  //  AppState.addEventListener("change", handleAppStateChange)
  //  
  //  return () => {
  //    AppState.removeEventListener("change", handleAppStateChange)
  //  }
  //}, [])
//
  //const handleAppStateChange = (nextAppState) => {
  //  if (
  //    appState.current.match(/inactive|background/) &&
  //    nextAppState === "active"
  //  ) {
  //    //console.log("App has come to the foreground!");
  //  }
//
  //  appState.current = nextAppState
  //  setAppStateVisible(appState.current)
  //}
    
  /*
  componentDidMount() {
    this.didFocusSubscription = this.props.navigation.addListener(
      'didFocus',
      () => {
        this.setState({
          video: this.props.video,
        });
        //console.log(this.props.video);
      },
    );
  }

  componentWillUnmount() {
    this.didFocusSubscription.remove();
  }
  */

  const handlePlay = () => {
    switch (Platform.OS) {
      case 'ios':
        player.current.presentFullscreenPlayer()
        break
      case 'android':
        navigation.navigate('VideoPlayer', { video: video })
        break
    }
  }

  if (!video) {
    return <View />
  }

  return (
    <>
      {video.video && video.video.downloadURL && (
        <TouchableOpacity onPress={handlePlay}>
          <>
            <VideoThumbnail video={video} />
            <View style={styles.iconContainer}>
              <Icon
                type="Entypo"
                name="controller-play"
                style={styles.playIcon}
              />
            </View>

            {/* This video is hidden but presents when handlePlay() is called */}
            <Video
              source={{uri: video.video.downloadURL}}
              ref={player}
              paused={paused}
              onFullscreenPlayerDidPresent={() => {
                setPaused(false)
              }}
              style={styles.hiddenFullScreenVideo}
            />
          </>
        </TouchableOpacity>
      )}
    </>
  )
}

const styles = StyleSheet.create({
  iconContainer: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  hiddenFullScreenVideo: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    height: 0,
    width: 0,
  },
  playIcon: {
    color: 'white',
    fontSize: 64,
    margin: 0,
    padding: 0,
    width: 64,
    height: 64,
  },
});

export default VideoPreview
