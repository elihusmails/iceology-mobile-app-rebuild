import React from 'react';

import firebase from '@react-native-firebase/app';
import '@react-native-firebase/firestore';
//import { GiftedChat } from "react-native-gifted-chat";

class VideoMessages extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      messages: [],
    };

    this.sendMessage = this.sendMessage.bind(this);
  }

  componentDidMount() {
    const video = this.props.video;

    this.messagesRef = firebase
      .firestore()
      .collection('videos')
      .doc(video.id)
      .collection('messages');

    this.messagesUnsubscribe = this.messagesRef
      .orderBy('createdAt', 'desc')
      .onSnapshot((querySnapshot) => {
        let messages = [];
        querySnapshot.forEach((doc) => {
          // Maybe dynamically detect the user _id here by checking
          // the "role" (below) instead of using hardcoded integers. For now, this works.
          messages.push({_id: doc.id, ...doc.data()});
        });

        //messages.push({
        //  _id: Math.round(Math.random() * 1000000),
        //  text: "You are officially rocking GiftedChat.",
        //  createdAt: new Date(),
        //  system: true
        //});

        this.setState({messages: messages});
      });
  }

  componentWillUnmount() {
    this.messagesUnsubscribe();
  }

  sendMessage(messages) {
    messages.forEach((message) => {
      const {text, user} = message;

      const data = {
        text,
        user,
        createdAt: firebase.firestore.FieldValue.serverTimestamp(),
      };

      const newMessageRef = this.messagesRef.doc();
      newMessageRef.set(data);
    });
  }

  //renderDay() {
  //  if (this.props.currentMessage.createdAt) {
  //    const dayProps = this.getInnerComponentProps();
  //    if (this.props.renderDay) {
  //      return this.props.renderDay(dayProps);
  //    }
  //    return <Day {...dayProps} />;
  //  }
  //  return null;
  //}

  render() {
    //const disable = false;
    //return (
    //  <View>
    //    {/*<GiftedChat
    //    messages={this.state.messages}
    //    onSend={this.sendMessage}
    //    user={{
    //      _id: firebaseCurrentUser().uid,
    //      uid: firebaseCurrentUser().uid,
    //      role: "athlete"
    //    }}
    //    isAnimated={true}
    //    multiline={true} //style={{ marginBottom: 48 }} //bottomOffset={48}
    //    renderInputToolbar={disable ? () => null : undefined}
    //  />*/}
    //  </View>
    //);
  }
}

export default VideoMessages;
