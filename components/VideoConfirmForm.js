import React from "react"
import { View } from "react-native"
import { Formik } from "formik"
import { compose } from "recompose"
import {
  handleTextInput,
  withFormikControl,
  withNextInputAutoFocusForm,
  withNextInputAutoFocusInput
} from "react-native-formik"
import * as Yup from "yup"

import Form from "./Form"
import FormTextInput from "./FormTextInput"
import FormPickerInput from "./FormPickerInput"

import Spacer from "./Spacer"
import PrimaryButton from "./Buttons"

const validationSchema = Yup.object().shape({
  title: Yup.string().required(),
  message: Yup.string().required(),
  identifier: Yup.string()
})

const VideoConfirmForm = ({ onSubmit, initialValues }) => {
  return (
    <Form
      initialValues={initialValues}
      enableReinitialize={true}
      validationSchema={validationSchema}
      onSubmit={values => onSubmit(values)}
    >
      {({ handleSubmit, values, errors }) => {
        return (
          <View>
            <FormTextInput
              name="title" label="Video title" error={errors.title}
              autoCapitalize="words"
            />
  
            <FormTextInput
              name="message" label="What would you like Pro feedback on?" error={errors.message}
              maxLength={280}
            />
  
            <FormTextInput
              name="identifier" label="How can we identify you in the video?" error={errors.identifier}
            />
  
            <Spacer height={18} />
  
            <PrimaryButton onPress={handleSubmit} text="Submit Video to Pro" />
          </View>
        )
      }}
    </Form>
  )
}

export default VideoConfirmForm