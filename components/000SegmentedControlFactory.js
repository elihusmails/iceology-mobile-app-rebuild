import React from "react";
import { SegmentedControlIOS, Text, View } from "react-native";
import t from "tcomb-form-native";

var Select = t.form.Select;

// extend the base Component
class SegmentedControlFactory extends Select {
  getTemplate() {
    return function(locals) {
      if (locals.hidden) {
        return null;
      }

      const stylesheet = locals.stylesheet;
      let formGroupStyle = stylesheet.formGroup.normal;
      let controlLabelStyle = stylesheet.controlLabel.normal;
      //let selectStyle = stylesheet.select.normal;
      let helpBlockStyle = stylesheet.helpBlock.normal;
      let errorBlockStyle = stylesheet.errorBlock;

      if (locals.hasError) {
        formGroupStyle = stylesheet.formGroup.error;
        controlLabelStyle = stylesheet.controlLabel.error;
        selectStyle = stylesheet.select.error;
        helpBlockStyle = stylesheet.helpBlock.error;
      }

      const label = locals.label ? (
        <Text style={controlLabelStyle}>{locals.label}</Text>
      ) : null;
      const help = locals.help ? (
        <Text style={helpBlockStyle}>{locals.help}</Text>
      ) : null;
      const error =
        locals.hasError && locals.error ? (
          <Text accessibilityLiveRegion="polite" style={errorBlockStyle}>
            {locals.error}
          </Text>
        ) : null;

      const options = Array.from(locals.options);
      const optionLabels = options.map(o => o.text);
      const selectedIndex = optionLabels.indexOf(locals.value);
      //console.error(selectedIndex);

      return (
        <View style={formGroupStyle}>
          {label}
          <SegmentedControlIOS
            values={optionLabels} //values={["One", "Two"]}
            selectedIndex={
              Number.isInteger(selectedIndex) ? selectedIndex : null
            }
            tintColor="#ffffff"
            enabled={!locals.disabled}
            onValueChange={label => {
              const selectedIndex = optionLabels.indexOf(label);
              const selectedValue = options[selectedIndex].value;

              const { onChange, value } = locals;
              if (selectedValue !== value) {
                onChange(selectedValue);
              }
            }}
          />
          {help}
          {error}
        </View>
      );
    };
  }

  //getOptions() {
  //  var options = super.getOptions();
  //  return options;
  //}

  getLocals() {
    var locals = super.getLocals();
    return locals;
  }

  handleValueChange(value) {}
}

// as example of transformer: this is the default transformer for textboxes
//SegmentedControl.transformer = {
//  format: value => (Nil.is(value) ? null : value),
//  parse: value =>
//    (t.String.is(value) && value.trim() === "") || Nil.is(value) ? null : value
//};

export default SegmentedControlFactory;

//var Person = t.struct({
//    name: t.String
//});
//
//var options = {
//    fields: {
//        name: {
//            factory: MyComponent
//        }
//    }
//};
