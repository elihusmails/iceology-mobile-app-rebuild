import React from "react";
import { View } from "react-native";

class Spacer extends React.Component {
  render() {
    return <View style={{ height: this.props.height }} />;
  }
}

export default Spacer;
