import React from "react";
import { View } from "react-native";
import _ from "lodash";

import { H2, BodyText, LabelText } from "../shared/typography";

import Spacer from "../components/Spacer";

const UserProfile = ({profile}) => {
  const inchesToHeight = (heightInInches) => {
    const feet = Math.floor(heightInInches / 12);
    const inches = heightInInches % 12;
    return `${feet}' ${inches}"`;
  }

  return (
    <View>
      <LabelText>Name</LabelText>
      <BodyText style={{ marginTop: 0, marginBottom: 0 }}>{profile.name}</BodyText>

      <Spacer height={24} />

      <View
        style={{
          flex: 1,
          flexDirection: "row",
          justifyContent: "space-between"
        }}
      >
        <View>
          <LabelText>Birth Year</LabelText>
          <BodyText>{profile.birthYear}</BodyText>
        </View>

        <View>
          <LabelText>Gender</LabelText>
          <BodyText>{profile.gender}</BodyText>
        </View>

        <View>
          <LabelText>Height</LabelText>
          <BodyText>{profile.height && inchesToHeight(profile.height)}</BodyText>
        </View>

        <View>
          <LabelText>Weight</LabelText>
          <BodyText>{profile.weight} lbs.</BodyText>
        </View>
      </View>
    </View>
  )
}

export default UserProfile