import React, { Component } from "react";
import styled, { css } from "@emotion/native";

import { font } from "../shared/utilities";
import { lightBlue, contentPadding } from "../shared/constants";

const ButtonContainer = styled.TouchableOpacity({
  marginHorizontal: contentPadding,
})

const ButtonText = styled.Text(props => ({
  ...font({
    family: Platform.select({ ios: "Gilroy", android: "Roboto" }),
    weight: props.primary ? "Bold" : "Regular",
    size: 17
  }),
  color: lightBlue
}))

const NavigationButton = ({ onPress, primary, children }) => {  
  return (
    <ButtonContainer onPress={onPress}>
      <ButtonText primary={primary}>{children}</ButtonText>
    </ButtonContainer>
  )
}

export default NavigationButton