import React, { useRef, useState } from 'react'
import {
  Dimensions,
  //Image,
  Platform,
  StyleSheet,
  //Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {Icon} from 'native-base';
import Video from 'react-native-video';

import VideoThumbnail from '../components/VideoThumbnail';

//const {windowHeight, windowWidth} = Dimensions.get('window');

const VideoPreviewInline = ({ videoURI }) => {
  const player = useRef(null)
  const [paused, setPaused] = useState(false)
  const [progress, setProgress] = useState(false)
  const [duration, setDuration] = useState(null)

  const handleVideoProgress = ({ currentTime }) => {
    setProgress(currentTime)
  }

  const handleVideoLoad = ({ duration }) => {
    setDuration(duration)
  }
  
  const {width} = Dimensions.get('window')

  const styles = StyleSheet.create({
    container: {
      backgroundColor: '#00000080',
      borderRadius: 8,
      overflow: 'hidden'
    },
    video: {
      height: width * 2/3,
    },
    progressBar: {
      position: 'absolute',
      bottom: 0,
      left: 0,
      right: 0,
      height: 6,
      backgroundColor: '#FFFFFF80',
      width: `${progress / duration * 100}%`
    }
  })

  return (
    <View style={styles.container}>
      <Video
        source={{uri: videoURI}}
        ref={player}
        paused={paused}
        onProgress={handleVideoProgress}
        onLoad={handleVideoLoad}
        repeat={true}
        resizeMode={"contain"}
        style={styles.video}
      />
      <View style={styles.progressBar}></View>
    </View>
  )
}

export default VideoPreviewInline
