import React from "react";
import { ActivityIndicator, Modal, Platform, Text } from "react-native";
import styled from "@emotion/native";

import { contentPadding } from "../shared/constants";

const LayoutContainer = styled.View({
  padding: contentPadding,
  backgroundColor: "#000000cc",
  flex: 1,
  top: 0,
  bottom: 0,
  justifyContent: "center",
  alignItems: "center"
});

const ProgressModal = ({ visible, text, ...otherProps }) => {
  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={visible}
      {...otherProps}
    >
      <LayoutContainer>
        <ActivityIndicator size="large" color="#ffffff" />
        <Text style={{ color: '#ffffff', marginTop: 8 }}>{text}</Text>
      </LayoutContainer>
    </Modal>
  );
}

export default ProgressModal
