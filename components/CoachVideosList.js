import React from 'react';
import {FlatList, View} from 'react-native';
import styled from '@emotion/native';
import moment from 'moment';

import {textColor} from '../shared/constants';
import {font} from '../shared/utilities';

import VideoStatus from '../components/VideoStatus';
import VideoThumbnail from '../components/VideoThumbnail';
import {H3, BodyText, MediumText} from '../shared/typography'

const TouchableArea = styled.TouchableOpacity({
  padding: 0,
})

const VideoLayout = styled.View({
  flex: 1,
  flexDirection: 'row',
  justifyContent: 'flex-start',
  margin: 16,
})

const VideoTitle = styled.Text({
  ...font({
    size: 20,
    weight: 'Bold',
  }),
  color: '#ffffff',
  marginTop: 4,
  marginBottom: 0,
})

const VideoTimestamp = styled.Text({
  marginBottom: 0,
  color: textColor,
  ...font({weight: 'Regular', size: 14}),
})

const ThumbnailBorder = styled.View({
  borderWidth: 0,
  padding: 3,
  backgroundColor: '#ffffff80',
  borderRadius: 8,
  marginRight: 12,
})

class CoachVideosList extends React.Component {
  render() {
    return (
      <FlatList
        data={this.props.videos}
        style={{paddingHorizontal: 8}}
        contentContainerStyle={{paddingVertical: 8}}
        ListHeaderComponent={
          () => (<BodyText style={{ margin: 16 }}>{this.props.summary}:</BodyText>)
        }
        keyExtractor={video => video.id}
        renderItem={({item: video}) => {
          //const user = video.uid;

          return (
            <TouchableArea onPress={() => this.props.handleVideoPress(video)}>
              <VideoLayout>
                <VideoThumbnail video={video} />

                <View>
                  <VideoTitle>{video.title}</VideoTitle>
                  <VideoTimestamp>
                    {video.createdAt &&
                      moment.unix(video.createdAt.seconds).fromNow()}
                  </VideoTimestamp>
                  <VideoStatus status={video.status} />
                </View>
              </VideoLayout>
            </TouchableArea>
          );
        }}
      />
    );
  }
}

export default CoachVideosList;
