import React from "react"
import { View } from "react-native"
import { Formik } from "formik"
import { compose } from "recompose"
import {
  handleTextInput,
  withFormikControl,
  withNextInputAutoFocusForm,
  withNextInputAutoFocusInput
} from "react-native-formik"
import * as Yup from "yup"

import Form from "./Form"
import FormTextInput from "./FormTextInput"
import FormPickerInput from "./FormPickerInput"

import Spacer from "./Spacer"
import PrimaryButton from "./Buttons"

const validationSchema = Yup.object().shape({
  message: Yup.string().required(),
})

const AthleteVideoFollowupForm = ({ onSubmit, initialValues }) => {
  return (
    <Form
      initialValues={initialValues}
      enableReinitialize={true}
      validationSchema={validationSchema}
      onSubmit={values => onSubmit(values)}
    >
      {({ handleSubmit, values, errors }) => {
        return (
          <View>  
            <FormTextInput
              name="message"
              label="Followup Question for Pro"
              placeholder="What would you like your Pro to follow up on? Please be as specific as possible."
              error={errors.message}
              maxLength={280}
              multiline
              numberOfLines={4}
            />
    
            <PrimaryButton onPress={handleSubmit} text="Submit Request" />
          </View>
        )
      }}
    </Form>
  )
}

export default AthleteVideoFollowupForm