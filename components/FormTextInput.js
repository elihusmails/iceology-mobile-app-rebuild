import React from 'react'
import { compose } from 'recompose'
import { TextField } from 'react-native-ui-lib'
import makeInput, {
  handleTextInput,
  withFormikControl,
} from 'react-native-formik'

import { textColor, errorColor } from '../shared/constants'

const FormikTextInput = withFormikControl(compose(handleTextInput)(TextField));

export default FormTextInput = ({ props, label, value, error, ...otherProps }) => {
  return (
    <FormikTextInput
      enableErrors={true}
      error={error}
      errorColor={errorColor}
      placeholder={label}
      placeholderTextColor={'rgba(255, 255, 255, 0.5)'}
      floatingPlaceholder={true}
      floatingPlaceholderColor={{
        default: 'rgba(255, 255, 255, 0.75)', error: errorColor, focus: 'rgba(255, 255, 255, 0.75)', disabled: '#FFFFFF'
      }}
      floatingPlaceholderStyle={{
        fontFamily: 'Gilroy'
      }}
      underlineColor={{
        default: 'rgba(255, 255, 255, 0.25)', error: errorColor, focus: '#FFFFFF', disabled: '#CCCCCC'
      }}
      floatOnFocus={true}
      //value={value}
      style={{
        color: textColor,
      }}
      {...otherProps}
    />
  )
}