import React from "react";
import { View } from "react-native";
import _ from "lodash";

import { H2, BodyText, LabelText } from "../shared/typography";

import Spacer from "../components/Spacer";

const UserProfile = ({profile}) => {
  const inchesToHeight = (heightInInches) => {
    const feet = Math.floor(heightInInches / 12);
    const inches = heightInInches % 12;
    return `${feet}' ${inches}"`;
  }

  return (
    <View>
      <LabelText>Name</LabelText>
      <H2 style={{ marginTop: 0, marginBottom: 0 }}>{profile.name}</H2>

      <Spacer height={24} />

      <View
        style={{
          flex: 1,
          flexDirection: "row",
          justifyContent: "space-between"
        }}
      >
        <View>
          <LabelText>Birth Year</LabelText>
          <BodyText>{profile.birthYear}</BodyText>
        </View>

        <View>
          <LabelText>Gender</LabelText>
          <BodyText>{profile.gender}</BodyText>
        </View>

        <View>
          <LabelText>Height</LabelText>
          <BodyText>{profile.height && inchesToHeight(profile.height)}</BodyText>
        </View>

        <View>
          <LabelText>Weight</LabelText>
          <BodyText>{profile.weight} lbs.</BodyText>
        </View>
      </View>

      <Spacer height={24} />

      <View
        style={{
          flex: 1,
          flexDirection: "row",
          justifyContent: "space-between"
        }}
      >
        <View>
          <LabelText>Player #</LabelText>
          <BodyText>
            {profile.playerNumber}
          </BodyText>
        </View>

        <View>
          <LabelText>Position</LabelText>
          <BodyText>{profile.position}</BodyText>
        </View>

        <View>
          <LabelText>Shoots</LabelText>
          <BodyText>{profile.shoots}</BodyText>
        </View>
      </View>

      <Spacer height={24} />

      <View>
        <LabelText>Skills</LabelText>
        <BodyText>{profile.skills || "—"}</BodyText>
      </View>

      <Spacer height={24} />

      <View>
        <LabelText>Experience</LabelText>
        <BodyText>{profile.experience || "—"}</BodyText>
      </View>

      <Spacer height={24} />

      <View>
        <LabelText>Strengths</LabelText>
        <BodyText>{profile.strengths || "—"}</BodyText>
      </View>

      <Spacer height={24} />

      <View>
        <LabelText>Weaknesses</LabelText>
        <BodyText>{profile.weaknesses || "—"}</BodyText>
      </View>
    </View>
  )
}

export default UserProfile