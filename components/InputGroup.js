import React from "react";
import { StyleSheet, Text, View } from "react-native";

class InputGroup extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <TouchableHighlight
          onPress={() => this._onPress(item)}
          onShowUnderlay={separators.highlight}
          onHideUnderlay={separators.unhighlight}
        >
          <View style={{ backgroundColor: "white" }}>
            <Text>{item.title}</Text>
          </View>
        </TouchableHighlight>
        <Text>Video {this.props.number}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  inputGroup: {
    borderWidth: 1,
    borderColor: "#ddd",
    borderRadius: 4
  }
});

export default InputGroup;
