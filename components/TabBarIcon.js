import React from 'react'
import {Image, View} from 'react-native'

const TabBarIcon = ({ focused, icon }) => {
  const icons = {
    'videos': require('../assets/images/tab-icon-videos.png'),
    'profile': require('../assets/images/tab-icon-profile.png'),
    'new-video': require('../assets/images/tab-icon-new-video.png'),
  }
  
  const iconSize = Platform.select({ ios: 32, android: 24 })
  
  const imageStyle = {
    width: iconSize,
    height: iconSize,
    opacity: focused ? 1 : 0.75,
  }
  
  return (
    <View>
      <Image source={icons[icon]} style={imageStyle} />
    </View>
  )
}

export default TabBarIcon