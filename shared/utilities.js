import React from 'react'
import { Dimensions, Platform } from 'react-native'
import _ from 'lodash'

const fonts = {
  Gilroy: {
    weights: {
      Thin: "200",
      UltraLight: "100",
      Light: "300",
      Regular: "400",
      Medium: "500",
      Semibold: "600",
      Bold: "700",
      Black: "800",
      Extrabold: "900"
    },
    styles: {
      Normal: "normal",
      Italic: "italic"
    }
  },
  Roboto: {
    weights: {
      Thin: "200",
      UltraLight: "100",
      Light: "300",
      Regular: "400",
      Medium: "500",
      Semibold: "600",
      Bold: "700",
      Black: "800",
      Extrabold: "900"
    },
    styles: {
      Normal: "normal",
      Italic: "italic"
    }
  },
};

export const font = (options = {}) => {
  let fontStyles;
  let { weight, style, size, lineHeight, family } = Object.assign(
    {
      weight: null,
      style: null,
      family: "Gilroy",
      size: null,
      lineHeight: 1.5
    },
    options
  );

  // Handle vw units
  if (size) {
    const pattern = /([\.\d]+)vw/;
    const match = pattern.exec(size);
    if (match !== null) {
      const units = match[1];
      const { width } = Dimensions.get("window");
      size = (units / 100) * width;
    }
  }

  const { weights, styles } = fonts[family];

  if (Platform.OS === "android") {
    weight = weights[weight] ? weight : "";
    style = styles[style] ? style : "";

    const suffix = weight + style;
    const fontFamilyWeightStyle = family + (suffix.length ? `-${suffix}` : "");

    fontStyles = {
      fontFamily: fontFamilyWeightStyle.toLowerCase(),
      fontSize: size,
      lineHeight: lineHeight * size
    };
  } else {
    weight = weights[weight] || "normal";
    style = styles[style] || "normal";

    fontStyles = {
      fontFamily: family,
      fontWeight: weight,
      fontStyle: style,
      fontSize: size,
      lineHeight: lineHeight * size
    };
  }

  return _.omitBy(fontStyles, _.isNil)
}

export const getObjectKeysByValue = (object, value) => {
  return Object.keys(object).filter(key => object[key] === value);
}
