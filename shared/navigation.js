import {Platform} from 'react-native'
import {contentPadding, darkBlue, highlightColor} from './constants';

export const navigationOptions = {
  headerTransparent: false,
  headerStyle: {
    backgroundColor: '#ffffff',
    shadowOpacity: 0,
    shadowOffset: {
      height: 0,
    },
    shadowRadius: 0,
    borderBottomWidth: 0,
    borderBottomColor: '#ffffff75',
    elevation: 0,
  },
  headerTintColor: darkBlue,
  headerTitleStyle: {
    marginHorizontal: contentPadding,
    fontFamily: 'Gilroy',
  },
  headerBackTitle: null,
  headerTitleAllowFontScaling: true
}

export const tabBarOptions = {
  //showLabel: false,
  activeTintColor: '#ffffff',
  inactiveTintColor: '#ffffff95',
  //safeAreaInset: { bottom: 'never' },
  style: {
    backgroundColor: highlightColor,
    borderTopHeight: 0,
    borderTopColor: 'transparent',
    shadowOpacity: 0,
    shadowOffset: {
      height: 0,
    },
    shadowRadius: 0,
    height: Platform.select({ ios: 100, android: 64 }),
    elevation: 0,
  },
  tabStyle: {
    paddingTop: Platform.select({ ios: 8, android: 8 }),
    paddingBottom: Platform.select({ ios: 0, android: 8 }),
  },
  labelStyle: {
    fontFamily: 'Gilroy',
    fontWeight: '500',
    fontSize: 13,
    textTransform: 'uppercase',
    margin: 0,
    marginTop: 0,
    padding: 0,
    letterSpacing: 0.25
  },
  allowFontScaling: false,
}