import {firebaseLogin} from './firebase.js'

export const emailPasswordlogin = async (email, password) => {
  const UserCredential = await firebaseLogin(email, password)
  return UserCredential
}