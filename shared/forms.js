import _ from "lodash";

import { font } from "./utilities";
import { darkBlue } from "./constants";

import { Platform } from "react-native";

const FONT_SIZE = 18;

const LABEL_SIZE = 14;
const LABEL_COLOR = "#0E223A" + "cc"; //textColor + "99";
const LABEL_WEIGHT = "Bold";

const INPUT_COLOR = darkBlue;
const ERROR_COLOR = "#ff0000";

const HELP_SIZE = FONT_SIZE;
const HELP_COLOR = "#ffffff";
const HELP_WEIGHT = "Regular";
const HELP_STYLE = "Italic";

const BORDER_COLOR = "transparent";
const DISABLED_COLOR = "#777777";
const BACKGROUND_COLOR = "#ffffff99"; //"#ffffff";
const DISABLED_BACKGROUND_COLOR = "#eeeeee";

export const formStylesheet = Object.freeze({
  fieldset: {},
  formGroup: {
    normal: {
      marginBottom: 12
    },
    error: {
      marginBottom: 12
    }
  },
  controlLabel: {
    normal: {
      color: LABEL_COLOR,
      ...font({
        weight: LABEL_WEIGHT,
        size: LABEL_SIZE
      }),
      letterSpacing: 1,
      marginBottom: 4,
      marginLeft: 8,
      textTransform: "uppercase"
    },
    error: {
      color: ERROR_COLOR,
      ...font({
        weight: LABEL_WEIGHT,
        size: LABEL_SIZE
      }),
      letterSpacing: 1,
      marginBottom: 4,
      marginLeft: 8,
      textTransform: "uppercase"
    }
  },
  helpBlock: {
    normal: {
      color: HELP_COLOR,
      ...font({
        size: HELP_SIZE,
        weight: HELP_WEIGHT,
        style: HELP_STYLE
      }),
      marginBottom: 2
    },
    // the style applied when a validation error occours
    error: {
      color: HELP_COLOR,
      fontSize: FONT_SIZE,
      marginBottom: 2
    }
  },
  errorBlock: {
    fontSize: FONT_SIZE,
    marginBottom: 2,
    color: ERROR_COLOR
  },
  textboxView: {
    normal: {},
    error: {},
    notEditable: {}
  },
  textbox: {
    normal: {
      color: INPUT_COLOR,
      fontSize: FONT_SIZE,
      height: 36,
      paddingVertical: Platform.OS === "ios" ? 7 : 0,
      paddingHorizontal: 7,
      borderRadius: 4,
      borderColor: BORDER_COLOR,
      borderWidth: 1,
      marginBottom: 5,
      backgroundColor: BACKGROUND_COLOR
    },
    // the style applied when a validation error occours
    error: {
      color: INPUT_COLOR,
      fontSize: FONT_SIZE,
      height: 36,
      paddingVertical: Platform.OS === "ios" ? 7 : 0,
      paddingHorizontal: 7,
      borderRadius: 4,
      borderColor: ERROR_COLOR,
      borderWidth: 1,
      marginBottom: 5,
      backgroundColor: BACKGROUND_COLOR
    },
    // the style applied when the textbox is not editable
    notEditable: {
      fontSize: FONT_SIZE,
      height: 36,
      paddingVertical: Platform.OS === "ios" ? 7 : 0,
      paddingHorizontal: 7,
      borderRadius: 4,
      borderColor: BORDER_COLOR,
      borderWidth: 1,
      marginBottom: 5,
      color: DISABLED_COLOR,
      backgroundColor: DISABLED_BACKGROUND_COLOR
    }
  },
  checkbox: {
    normal: {
      marginBottom: 4
    },
    // the style applied when a validation error occours
    error: {
      marginBottom: 4
    }
  },
  pickerContainer: {
    normal: {
      color: INPUT_COLOR,
      marginBottom: 4,
      borderRadius: 4,
      borderColor: BORDER_COLOR,
      borderWidth: 1,
      backgroundColor: BACKGROUND_COLOR
    },
    error: {
      color: INPUT_COLOR,
      marginBottom: 4,
      borderRadius: 4,
      borderColor: ERROR_COLOR,
      borderWidth: 1,
      backgroundColor: BACKGROUND_COLOR
    },
    open: {
      // Alter styles when select container is open
    }
  },
  select: {
    normal: Platform.select({
      android: {
        paddingLeft: 7,
        color: INPUT_COLOR
      },
      ios: {
        color: INPUT_COLOR
      }
    }),
    // the style applied when a validation error occours
    error: Platform.select({
      android: {
        paddingLeft: 7,
        color: ERROR_COLOR
      },
      ios: {}
    })
  },
  pickerTouchable: {
    normal: {
      height: 44,
      flexDirection: "row",
      alignItems: "center"
    },
    error: {
      height: 44,
      flexDirection: "row",
      alignItems: "center"
    },
    active: {
      borderBottomWidth: 1,
      borderColor: BORDER_COLOR
    },
    notEditable: {
      height: 44,
      flexDirection: "row",
      alignItems: "center",
      backgroundColor: DISABLED_BACKGROUND_COLOR
    }
  },
  pickerValue: {
    normal: {
      fontSize: FONT_SIZE,
      paddingLeft: 7,
      color: INPUT_COLOR
    },
    error: {
      fontSize: FONT_SIZE,
      paddingLeft: 7
    }
  },
  datepicker: {
    normal: {
      marginBottom: 4
    },
    // the style applied when a validation error occours
    error: {
      marginBottom: 4
    }
  },
  dateTouchable: {
    normal: {},
    error: {},
    notEditable: {
      backgroundColor: DISABLED_BACKGROUND_COLOR
    }
  },
  dateValue: {
    normal: {
      color: INPUT_COLOR,
      fontSize: FONT_SIZE,
      padding: 7,
      marginBottom: 5
    },
    error: {
      color: ERROR_COLOR,
      fontSize: FONT_SIZE,
      padding: 7,
      marginBottom: 5
    }
  },
  buttonText: {
    fontSize: 18,
    color: "white",
    alignSelf: "center"
  },
  button: {
    height: 36,
    backgroundColor: "#48BBEC",
    borderColor: "#48BBEC",
    borderWidth: 1,
    borderRadius: 8,
    marginBottom: 10,
    alignSelf: "stretch",
    justifyContent: "center"
  }
});
