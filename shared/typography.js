import React from 'react';
import {View} from 'react-native';
import styled from '@emotion/native';

import {font} from '../shared/utilities';
import {textColor} from '../shared/constants';
import Center from '../components/Center';

export const H1 = styled.Text({
  color: textColor,
  ...font({
    size: 48,
    weight: 'Extrabold',
    lineHeight: 1.25,
  }),
  marginTop: 24,
  marginBottom: 18,
});

export const H2 = styled.Text({
  color: textColor,
  ...font({
    size: 36,
    weight: 'Extrabold',
    lineHeight: 1.25,
  }),
  marginTop: 24,
  marginBottom: 12,
});

export const H3 = styled.Text({
  color: textColor,
  ...font({
    size: 24,
    weight: 'Bold',
    lineHeight: 1.25,
  }),
  marginTop: 4,
  marginBottom: 4,
});

export const BodyText = styled.Text({
  color: textColor,
  ...font({
    size: 18,
    weight: 'Regular',
  }),
  marginBottom: 8,
});

export const MediumText = styled.Text({
  color: textColor,
  ...font({
    size: 16,
    weight: 'Regular',
  }),
  marginBottom: 8,
});

export const SmallText = styled.Text({
  color: textColor,
  ...font({
    size: 14,
    weight: 'Regular',
  }),
  marginBottom: 8,
});

export const TextButton = ({onPress, align, children}) => {
  return (
    <TextButtonContainer align={align}>
      <TextButtonText onPress={onPress}>{children}</TextButtonText>
    </TextButtonContainer>
  )
}

export const TextButtonContainer = styled.View(({align = 'center'}) => {
  const alignMap = {
    'left': 'flex-start',
    'center': 'center',
    'right': 'flex-end',
  }
  
  return {
    alignSelf: alignMap[align],
    paddingVertical: 0,
    borderBottomWidth: 2,
    borderBottomColor: textColor + '66',
  }
})

export const TextButtonText = styled.Text({
  color: textColor,
  ...font({
    size: 16,
    weight: 'Bold',
  }),
  letterSpacing: 0.2,
  textTransform: 'uppercase',
  borderBottomWidth: 2,
  borderBottomColor: textColor + '66',
});

export const LabelText = styled.Text({
  color: 'rgba(255, 255, 255, 0.6)',
  ...font({
    size: 11,
    weight: 'Bold',
  }),
  textTransform: 'uppercase',
  letterSpacing: 0.2,
  padding: 0,
  lineHeight: 16,
});

export const LeadText = styled.Text({
  ...font({
    weight: 'Medium',
    size: 20,
    lineHeight: 1.25,
  }),
  color: textColor,
  marginTop: 0,
  marginBottom: 24,
  textAlign: 'center',
});

export const ErrorText = props => {
  return (
    <ErrorTextContainer>
      <ErrorTextText>{props.children}</ErrorTextText>
    </ErrorTextContainer>
  );
};

export const ErrorTextContainer = styled.View({
  paddingVertical: 6,
  paddingHorizontal: 12,
  backgroundColor: '#B7282B',
  borderRadius: 6,
});

export const ErrorTextText = styled.Text({
  color: 'white',
  ...font({
    size: 16,
    weight: 'Bold',
  }),
  letterSpacing: 0.2,
});

export const StatusText = styled.Text({
  color: textColor,
  ...font({
    size: 11,
    weight: 'Bold',
  }),
  textTransform: 'uppercase',
  letterSpacing: 0.2,
  padding: 0,
  lineHeight: 16,
});

export const TimestampText = styled.Text({
  color: textColor,
  ...font({
    size: 14,
    weight: 'Regular',
  }),
  marginBottom: 8,
});
