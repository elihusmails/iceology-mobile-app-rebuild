import { Platform } from 'react-native'
import Purchases from 'react-native-purchases'
import _ from 'lodash'
import moment from 'moment'

//import {isSubscribed, getPurchaseData, getAvailablePackages} from './purchases'
import { firebaseCurrentUser, firebaseGetVideos } from './firebase'
import { SUBSCRIPTIONS } from './constants'

//const formatPurchaseError = error => {
//  return Platform.select({
//    ios: error.userInfo.NSLocalizedDescription,
//    android: error.message,
//  })
//}

//export const getPurchaseData = async () => {
//  try {
//    const entitlements = await Purchases.getEntitlements()
//    const purchaserInfo = await Purchases.getPurchaserInfo()
//
//    return {
//      entitlements: entitlements,
//      purchaserInfo: purchaserInfo,
//    }
//  } catch (error) {
//    throw formatPurchaseError(error)
//  }
//}

export const getAvailablePackages = async () => {
  const offerings = await Purchases.getOfferings()
  
  if (offerings &&
      offerings.current &&
      offerings.current.availablePackages &&
      offerings.current.availablePackages.length > 0
  ) {
    return offerings.current.availablePackages
  }
  
  return null
}

export const getSubscriptions = async () => {
  try {
    const offerings = await Purchases.getOfferings()
    if (offerings.current !== null) {  
      //console.log(offerings.current)
      return offerings
    }
  } catch (error) {
    throw error
  }
}

export const purchaseSubscription = async productId => {
  try {
    const purchase = await Purchases.makePurchase(productId)
    return purchase
  } catch (error) {
    throw formatPurchaseError(error)
  }
}

export const restorePurchases = async () => {
  try {
    const purchases = await Purchases.restoreTransactions()
    return purchases
  } catch (error) {
    throw formatPurchaseError(error)
  }
}

export const isSubscribed = async () => {
  const purchaserInfo = await Purchases.getPurchaserInfo();
  
  return Object.entries(purchaserInfo.entitlements.active).length ? true : false
}

export const getSubscriptionUsage = async () => {
  const subscribed = await isSubscribed()
  if (!subscribed) return {}

  const purchaserInfo = await Purchases.getPurchaserInfo()
  const activeEntitlements = Object.values(purchaserInfo.entitlements.active)
  
  // Previously were doing this:
  // const activeEntitlement = activeEntitlements[0]
  // But it's possible that a user could have multiple subscriptions at once,
  // so we need to check *all* of their entitlements and take the biggest quota.
  
  const activeEntitlement = activeEntitlements[0]  

  const currentSubscriptionMetadata = _.find(SUBSCRIPTIONS, o => {
    return o.entitlement === activeEntitlement.identifier && o.active
  })
  
  //console.log({currentSubscriptionMetadata})
  const currentSubscriptionQuota = currentSubscriptionMetadata.quota

  const purchaseDate = moment(activeEntitlement.latestPurchaseDate).unix()
  const expiresDate = moment(activeEntitlement.expirationDate).unix()

  // Find any videos between
  const videos = await firebaseGetVideos(firebaseCurrentUser().uid)
  const videosInCurrentPeriod = videos.filter(video => {
    //const videoDate = moment(video.createdAt).unix()
    const videoDate = video.createdAt.seconds
    const inRange = _.inRange(videoDate, purchaseDate, expiresDate)
    return inRange
  }).length

  return {
    quota: currentSubscriptionQuota,
    used: videosInCurrentPeriod,
  }
}

export const getSubscriptionsUsage = async () => {
  const subscribed = await isSubscribed()
  if (!subscribed) return []

  const videos = await firebaseGetVideos(firebaseCurrentUser().uid)
  const purchaserInfo = await Purchases.getPurchaserInfo()
  const activeEntitlements = Object.values(purchaserInfo.entitlements.active)
  
  return activeEntitlements.map(activeEntitlement => {

    const currentSubscriptionMetadata = _.find(SUBSCRIPTIONS, o => {
      return o.entitlement === activeEntitlement.identifier && o.active
    })
    
    const currentSubscriptionQuota = currentSubscriptionMetadata.quota
  
    const purchaseDate = moment(activeEntitlement.latestPurchaseDate).unix()
    const expiresDate = moment(activeEntitlement.expirationDate).unix()
  
    const videosInCurrentPeriod = videos.filter(video => {
      const videoDate = video.createdAt.seconds
      return _.inRange(videoDate, purchaseDate, expiresDate)
    }).length
  
    return {
      quota: currentSubscriptionQuota,
      used: videosInCurrentPeriod,
    }
  })
}

//export const getExpirationDate = (purchaserInfo, entitlementId) => {
//  return purchaserInfo.expirationsForActiveEntitlements[entitlementId];
//};

//export const mapEntitlementsToProducts = entitlements => {
//  console.log('Object.entries(entitlements)');
//  console.log(Object.entries(entitlements));
//
//  let products = [];
//
//  Object.entries(entitlements).forEach(([entitlementId, entitlement]) => {
//    Object.entries(entitlement).forEach(([offeringId, offering]) => {
//      const product = Object.assign(offering, {
//        entitlementId: entitlementId,
//        offeringId: offeringId,
//      });
//
//      products.push(product);
//    });
//  });
//
//  return products;
//};

//export const sortByPrice = o => o.sort((a, b) => a.price < b.price);

//export const isActiveEntitlement = (purchaserInfo, entitlementId) => {
//  return (
//    purchaserInfo.activeEntitlements !== 'undefined' &&
//    purchaserInfo.activeEntitlements.includes(entitlementId)
//  );
//};