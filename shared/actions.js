import React, { useContext } from 'react'
import {Platform} from 'react-native'

import firebase from '@react-native-firebase/app'
import '@react-native-firebase/auth'
import '@react-native-firebase/firestore'

import {createThumbnail} from 'react-native-create-thumbnail'
import {
  firebaseGetUser,
  firebaseUploadThumbnail,
  firebaseUploadVideo,
} from './firebase'

export const uploadVideo = async (videoURI, uploadProgress) => {
  const videoRef = await firebaseUploadVideo(videoURI, uploadProgress)
  const videoDownloadURL = await videoRef.getDownloadURL()
  const videoMetadata = await videoRef.getMetadata()
  
  return {
    downloadURL: videoDownloadURL,
    metadata: videoMetadata
  }
}

export const uploadVideoThumbnail = async (videoURI) => {  
  const thumbnailURI = await generateVideoThumbnail(videoURI)
  if (!thumbnailURI) return {}

  const thumbnailRef = await firebaseUploadThumbnail(thumbnailURI);
  const thumbnailDownloadURL = await thumbnailRef.getDownloadURL()
  const thumbnailMetadata = await thumbnailRef.getMetadata()
  return {
    downloadURL: thumbnailDownloadURL,
    metadata: thumbnailMetadata      
  }
}

export const saveVideoData = async (user, videoFile, videoThumbnailFile, formData) => {
  const videosRef = firebase.firestore().collection('videos')
  const data = {
    uid: user.uid,
    video: videoFile,
    thumbnail: videoThumbnailFile,
    createdAt: firebase.firestore.FieldValue.serverTimestamp(),
    status: 'AWAITING_FEEDBACK',
    ...formData,
  }

  const videoDocRef = await videosRef.add(data)
  return videoDocRef;
}

export const submitVideoFeedback = async (user, video, messageText, messageType) => {
  await saveVideoMessage(user, video.id, messageText, messageType)
  await updateVideoStatus(video.id, 'RECEIVED_FEEDBACK')
}

export const requestVideoFollowup = async (user, video, messageText, messageType) => {
  await saveVideoMessage(user, video.id, messageText, messageType)
  await updateVideoStatus(video.id, 'AWAITING_FOLLOWUP')
}

export const submitVideoFollowup = async (user, video, messageText, messageType) => {
  await saveVideoMessage(user, video.id, messageText, messageType)
  await updateVideoStatus(video.id, 'COMPLETE')
}

export const saveVideoMessage = async (user, videoId, messageText, messageType) => {
  const firebaseUser = await firebaseGetUser(user.uid);
  const videosRef = firebase.firestore().collection('videos');
  const messagesRef = videosRef.doc(videoId).collection('messages');

  const data = {
    text: messageText,
    user: {
      role: firebaseUser.role,
      uid: user.uid,
    },
    type: messageType,
    createdAt: firebase.firestore.FieldValue.serverTimestamp(),
  };

  const messageDocRef = await messagesRef.add(data);
  return messageDocRef;
};

export const saveVideo = async (user, videoURI, formData, uploadProgress) => {
  const videoData = await uploadVideo(videoURI, uploadProgress)
  const videoThumbnailData = await uploadVideoThumbnail(videoURI)
  
  const videoDocRef = await saveVideoData(
    user,
    videoData,
    videoThumbnailData,
    formData
  )

  await saveVideoMessage(
    user,
    videoDocRef.id,
    formData.message,
    'USER_FEEDBACK_REQUEST',
  )
}

export const updateVideoStatus = (videoId, status) => {
  return new Promise((resolve, reject) => {
    firebase
      .firestore()
      .collection('videos')
      .doc(videoId)
      .update({status: status})
      .then((docRef) => {
        resolve(docRef);
      })
      .catch((err) => {
        reject(`Status update error: ${JSON.stringify(err)}`);
      });
  });
};

export const generateVideoThumbnail = (uri) => {
  if (Platform.OS === 'android') {
    return new Promise((resolve, reject) => {
      //const thumbnail = require('../assets/images/video-thumbnail-default.png');
      resolve(null);
    });
  }

  if (Platform.OS === 'ios') {
    return new Promise((resolve, reject) => {
      createThumbnail({
        url: uri,
        type: 'local',
      })
        .then((response) => {
          resolve(response.path);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }
};
