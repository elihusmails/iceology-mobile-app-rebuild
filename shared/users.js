import { userProfileValidationSchema } from "./formValidationSchemas";

export const isUserProfileComplete = user => {
  return new Promise((resolve, reject) => {
    userProfileValidationSchema
      .isValid(user.profile)
      .then(valid => {
        resolve(true);
      })
      .catch(error => {
        reject(false);
      });
  });
};