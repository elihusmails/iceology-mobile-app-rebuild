export const REVENUECAT_API_KEY = "iryvjemBaiUCOVHWEsUjptoMwLGDMhEI"
export const SENTRY_DSN = 'https://678430a0a07141be86a70c318a4fcf16@o968083.ingest.sentry.io/5919481'

export const SUBSCRIPTIONS = {
  "com.iceologyhockey.bronze.v2": {
    entitlement: "bronze",
    quota: 1,
    active: true
  },
  "com.iceologyhockey.silver.v2": {
    entitlement: "silver",
    quota: 4,
    active: true
  },
  "com.iceologyhockey.gold.v2": {
    entitlement: "gold",
    quota: 12,
    active: true
  }
}

export const darkBlue = "#1c4474"
export const lightBlue = "#33bae2"
export const orange = "#f79e02"
export const green = "#60C034"

export const primaryColor = darkBlue
export const highlightColor = green
export const textColor = '#FFFFFF'
export const darkTextColor = "#0E223A" + "cc" // https://meyerweb.com/eric/tools/color-blend/#1C4474:000000:3:hex
export const errorColor = 'rgb(255,36,63)'

export const navigationHeadroom = 48
export const contentPadding = 24
