import firebase from '@react-native-firebase/app'
import '@react-native-firebase/auth'
import '@react-native-firebase/firestore'
import '@react-native-firebase/storage'
import '@react-native-firebase/messaging'

import _ from 'lodash'

// 5.15.0 - [Firebase / Firestore][I - FST000001]
// The behavior for system Date objects stored in
// Firestore is going to change AND YOUR APP MAY BREAK.
let db = firebase.firestore()
let settings = db.settings
settings.areTimestampsInSnapshotsEnabled = true
db.settings = settings

export const firebaseCurrentUser = () => {
  return firebase.auth().currentUser
}

//export const firebaseRegister = async (email, password) => {
//  return new Promise((resolve, reject) => {
//    firebase
//      .auth()
//      .createUserWithEmailAndPassword(email, password)
//      .then((UserCredential) => {
//        resolve(UserCredential);
//      })
//      .catch((error) => {
//        const errors = {
//          'auth/invalid-email': 'Email address is invalid.',
//          'auth/email-already-in-use': 'That email address is already in use.',
//          'auth/operation-not-allowed':
//            'Signups are currently disabled. Try again later.',
//          'auth/weak-password': 'Password is too weak.',
//        }
//        reject(errors[error.code])
//      })
//  })
//}

export const firebaseCreateUser = async (uid, role = 'athlete') => {
  // Bootstrap a sparse profile containing only the user type (ex: athlete/coach).
  // It would be better to handle this with custom "claims" in Firebase.
  const data = {uid: uid, role: role, profile: {}}

  return new Promise((resolve, reject) => {
    firebase
      .firestore()
      .doc(`users/${uid}`)
      .set(data)
      .then((response) => {
        resolve(response);
      })
      .catch((err) => {
        reject(JSON.stringify(err))
      })
  })
}

export const firebaseDeleteCurrentUser = async (deleteData = false) => {
  var user = firebase.auth().currentUser

  if (deleteData === true) {
    // TODO: delete user's data from database
  }

  await firebaseDeleteUser(user.uid)
  // TODO: convert all this Firestore Promise shit to async/await
  // https://medium.com/@vladfr/use-cloud-firestore-with-async-bce875af0183

  return new Promise((resolve, reject) => {
    user
      .delete()
      .then((response) => {
        resolve(response)
      })
      .catch((err) => {
        reject(JSON.stringify(err))
      })
  })
}

//export const firebaseLogin = (email, password) => {
//  return new Promise((resolve, reject) => {
//    firebase
//      .auth()
//      .signInWithEmailAndPassword(email, password)
//      .then((UserCredential) => {
//        resolve(UserCredential)
//      })
//      .catch((error) => {
//        const errors = {
//          'auth/invalid-email': 'Email address is invalid.',
//          'auth/user-disabled': 'User is disabled.',
//          'auth/user-not-found': 'User not found.',
//          'auth/wrong-password': 'Incorrect username or password.',
//        }
//        reject(errors[error.code])
//      })
//  })
//}

//export const firebaseSendPasswordResetEmail = (email) => {
//  
//  console.log({ email })
//  
//  return new Promise((resolve, reject) => {
//    firebase
//      .auth()
//      .sendPasswordResetEmail(email)
//      .then(() => {
//        resolve()
//      })
//      .catch((error) => {
//        const errors = {
//          'auth/invalid-email': 'Email address is invalid.',
//          'auth/user-not-found': 'No user found with that email address.',
//        };
//        reject(errors[error.code])
//      })
//  })
//}

//export const firebaseLogout = () => {
//  return new Promise((resolve, reject) => {
//    firebase
//      .auth()
//      .signOut()
//      .then(() => {
//        resolve()
//      })
//      .catch((error) => {
//        reject(error)
//      })
//  })
//}

export const firebaseGetUser = (uid) => {
  return new Promise((resolve, reject) => {
    firebase
      .firestore()
      .doc(`users/${uid}`)
      .get()
      .then((response) => {
        var user = response.data()
        user.uid = uid // Include the request uid with the response object
        resolve(user)
      })
      .catch((err) => {
        reject(err)
      })
  })
}

export const firebaseDeleteUser = async (uid) => {
  return new Promise((resolve, reject) => {
    firebase
      .firestore()
      .doc(`users/${uid}`)
      .delete()
      .then((response) => {
        resolve(response)
      })
      .catch((err) => {
        reject(JSON.stringify(err))
      })
  })
}

export const firebaseUpdateUser = async (uid, userData) => {
  return new Promise((resolve, reject) => {
    firebase
      .firestore()
      .doc(`users/${uid}`)
      .set(userData, {merge: true})
      .then((response) => {
        resolve(response)
      })
      .catch((err) => {
        reject(JSON.stringify(err))
      })
  })
}

export const firebaseUpdateUserProfile = async (uid, profileData) => {
  return new Promise((resolve, reject) => {
    firebase
      .firestore()
      .doc(`users/${uid}`)
      .set({profile: profileData}, {merge: true})
      .then((response) => {
        resolve(response)
      })
      .catch((err) => {
        reject(JSON.stringify(err))
      })
  })
}

export const firebaseUpdateUserOnboarded = async (uid, onboarded) => {
  return new Promise((resolve, reject) => {
    firebase
      .firestore()
      .doc(`users/${uid}`)
      .set({onboarded: onboarded}, {merge: true})
      .then((response) => {
        resolve(response)
      })
      .catch((err) => {
        reject(JSON.stringify(err))
      })
  })
}

export const firebaseGetVideos = (uid) => {
  return new Promise((resolve, reject) => {
    firebase
      .firestore()
      .collection('videos')
      .where('uid', '==', uid)
      .get()
      .then((response) => {
        let videos = []

        response.forEach((doc) => {
          videos.push({
            id: doc.id,
            ...doc.data(),
          })
        })

        const videosSorted = videos.sort((a, b) => b.createdAt > a.createdAt)

        resolve(videosSorted)
      })
      .catch((err) => {
        reject(JSON.stringify(err))
      })
  })
}

export const firebaseUploadThumbnail = (thumbnailURI) => {
  return new Promise((resolve, reject) => {
    const thumbnailFileName = thumbnailURI.split(/[\\/]/).pop()
    const ref = firebase.storage().ref('thumbnails/' + thumbnailFileName)

    ref.putFile(thumbnailURI)
      .then(async (response) => {
        //console.log("firebaseUploadThumbnail:")
        //console.log({response})
        resolve(ref)
      })
      .catch((err) => {
        reject(JSON.stringify(err))
      });
  });
};

export const firebaseUploadVideo = async (uri, progressCallback) => {
  return new Promise((resolve, reject) => {
    //console.log({uri})
    const videoFileName = uri.split(/[\\/]/).pop()
    const ref = firebase.storage().ref('videos/' + videoFileName)
    const uploadTask = ref.putFile(uri)

    const unsubscribe = uploadTask.on(
      firebase.storage.TaskEvent.STATE_CHANGED,
      (snapshot) => {
        progressCallback(snapshot.bytesTransferred / snapshot.totalBytes)
      },
      (error) => {
        unsubscribe()
        reject(error)
      },
      (response) => {
        //console.log("firebaseUploadVideo:")
        //console.log({response})
        resolve(ref)
      },
    )
  })
}

//export const firebaseGetMessagingToken = async () => {
//  const token = await firebase.messaging().getToken()
//  return (token) ? token : false
//}

export const firebaseCheckMessagingPermission = async () => {
  return new Promise((resolve, reject) => {
    firebase
      .messaging()
      .hasPermission()
      .then((enabled) => {
        return enabled
      })
  })
}

export const firebaseRequestMessagingPermission = async () => {
  const authStatus = await messaging().requestPermission()
  const enabled =
    authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
    authStatus === messaging.AuthorizationStatus.PROVISIONAL

  //firebase
  //  .messaging()
  //  .requestPermission()
  //  .then((token) => {
  //    resolve(token)
  //  })
  //  .catch((error) => {
  //    reject(error)
  //  })
}

export const firebaseUpdateUserSubscription = async (uid, subscriptionData) => {
  return new Promise((resolve, reject) => {
    firebase
      .firestore()
      .doc(`users/${uid}`)
      .set({subscription: subscriptionData}, {merge: true})
      .then((response) => {
        resolve(response)
      })
      .catch((err) => {
        reject(JSON.stringify(err))
      })
  })
}

export const firebaseFormatError = (error) => {
  //return `[${error.code}] ${error.message}`
  return `${error.message}`
}