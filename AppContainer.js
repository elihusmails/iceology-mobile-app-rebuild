import React from 'react';
import {Image, View} from 'react-native';

import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {createStackNavigator} from 'react-navigation-stack';
//import createSwitchNavigator from 'react-navigation-stack';

import OnboardingScreen from './screens/OnboardingScreen';
import AuthDetectionScreen from './screens/AuthDetectionScreen';
import AuthLoginScreen from './screens/AuthLoginScreen';
import AuthRegisterAccountScreen from './screens/AuthRegisterAccountScreen';
import AuthForgotPasswordScreen from './screens/AuthForgotPasswordScreen';
import AuthRegisterUserTypeScreen from './screens/AuthRegisterUserTypeScreen';
import AuthRegisterSubscriptionScreen from './screens/AuthRegisterSubscriptionScreen';
import AuthRegisterProfileScreen from './screens/AuthRegisterProfileScreen';

import VideoPlayerScreen from './screens/VideoPlayerScreen';

// Athlete
import VideosListScreen from './screens/VideosScreen';
import VideoDetailScreen from './screens/VideoDetailScreen';
import VideoFeedbackScreen from './screens/VideoFeedbackScreen';
import VideoFollowupScreen from './screens/VideoFollowupScreen';
import NewVideoScreen from './screens/NewVideoScreen';
import NewVideoConfirmScreen from './screens/NewVideoConfirmScreen';
import ProfileScreen from './screens/ProfileScreen';
import ProfileEditScreen from './screens/ProfileEditScreen';
import AccountSubscriptionScreen from './screens/AccountSubscriptionScreen';

// Coach
import CoachVideosListScreen from './screens/coach/CoachVideosListScreen';
import CoachVideoDetailScreen from './screens/coach/CoachVideoDetailScreen';
import CoachVideoUserScreen from './screens/coach/CoachVideoUserScreen';
import CoachVideoFeedbackScreen from './screens/coach/CoachVideoFeedbackScreen';
import CoachVideoFollowupScreen from './screens/coach/CoachVideoFollowupScreen';
import CoachProfileScreen from './screens/coach/CoachProfileScreen';

import {darkBlue, highlightColor} from './shared/constants';

const navigationOptions = {
  headerTransparent: false,
  headerStyle: {
    backgroundColor: '#ffffff',
    shadowOpacity: 0,
    shadowOffset: {
      height: 0,
    },
    shadowRadius: 0,
    borderBottomWidth: 0,
    borderBottomColor: '#ffffff75',
    elevation: 0,
  },
  headerTintColor: darkBlue,
  headerTitleStyle: {
    fontFamily: 'Gilroy',
  },
  headerBackTitle: null,
  //headerTitleAllowFontScaling: false
};

const tabBarOptions = {
  showLabel: false,
  activeTintColor: '#ffffff',
  inactiveTintColor: '#ffffff95',
  //safeAreaInset: { bottom: 'never' },
  style: {
    backgroundColor: highlightColor,
    borderTopHeight: 0,
    borderTopColor: 'transparent',
    shadowOpacity: 0,
    shadowOffset: {
      height: 0,
    },
    shadowRadius: 0,
    height: 64,
    elevation: 0,
  },
  tabStyle: {
    paddingTop: 0,
    paddingBottom: 0,
  },
  labelStyle: {
    fontFamily: 'Gilroy',
    fontWeight: '500',
    fontSize: 14,
    textTransform: 'uppercase',
    margin: 0,
  },
  allowFontScaling: false,
};

class TabBarIcon extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const icons = {
      videos: require('./assets/images/tab-icon-videos.png'),
      profile: require('./assets/images/tab-icon-profile.png'),
      'new-video': require('./assets/images/tab-icon-new-video.png'),
    };

    const imageStyle = {
      width: 32,
      height: 32,
      opacity: this.props.focused ? 1 : 0.75,
    };

    return (
      <View>
        <Image source={icons[this.props.icon]} style={imageStyle} />
      </View>
    );
  }
}

const VideosStack = createStackNavigator(
  {
    VideosList: VideosListScreen,
    VideoDetail: VideoDetailScreen,
    VideoFeedback: VideoFeedbackScreen,
    VideoFollowup: VideoFollowupScreen,
  },
  {
    defaultNavigationOptions: navigationOptions,
  },
);

const CoachVideosStack = createStackNavigator(
  {
    CoachVideosList: CoachVideosListScreen,
    CoachVideoDetail: CoachVideoDetailScreen,
    CoachVideoUser: CoachVideoUserScreen,
  },
  {
    defaultNavigationOptions: navigationOptions,
  },
);

const CoachFeedbackStack = createStackNavigator(
  {
    CoachVideoFeedback: CoachVideoFeedbackScreen,
  },
  {
    defaultNavigationOptions: navigationOptions,
  },
);

const CoachFollowupStack = createStackNavigator(
  {
    CoachVideoFollowup: CoachVideoFollowupScreen,
  },
  {
    defaultNavigationOptions: navigationOptions,
  },
);

const AthleteNewVideoStack = createStackNavigator(
  {
    NewVideoMain: NewVideoScreen,
    NewVideoConfirm: NewVideoConfirmScreen,
  },
  {
    defaultNavigationOptions: navigationOptions,
  },
);

const ProfileStack = createStackNavigator(
  {
    Profile: ProfileScreen,
  },
  {
    defaultNavigationOptions: navigationOptions,
  },
);

const CoachProfileStack = createStackNavigator(
  {
    CoachProfile: CoachProfileScreen,
  },
  {
    defaultNavigationOptions: navigationOptions,
  },
);

const ProfileEditStack = createStackNavigator(
  {
    ProfileEdit: ProfileEditScreen,
  },
  {
    defaultNavigationOptions: navigationOptions,
  },
);

const VideoPlayerStack = createStackNavigator(
  {
    VideoPlayer: VideoPlayerScreen,
  },
  {
    defaultNavigationOptions: navigationOptions,
  },
);

const AccountStack = createStackNavigator(
  {
    AccountSubscription: AccountSubscriptionScreen,
  },
  {
    defaultNavigationOptions: navigationOptions,
  },
);

const AthleteTabs = createBottomTabNavigator(
  {
    VideosTab: {
      screen: VideosStack,
      navigationOptions: {
        tabBarLabel: 'Videos',
        tabBarIcon: ({focused}) => (
          <TabBarIcon icon="videos" focused={focused} />
        ),
      },
    },
    NewVideoTab: {
      screen: View,
      navigationOptions: ({navigation}) => ({
        tabBarLabel: 'New Video',
        tabBarIcon: ({focused}) => (
          <TabBarIcon icon="new-video" focused={focused} />
        ),
        tabBarOnPress: ({navigation}) => {
          navigation.navigate('AthleteNewVideo');
        },
      }),
    },
    ProfileTab: {
      screen: ProfileStack,
      navigationOptions: {
        tabBarLabel: 'Profile',
        tabBarIcon: ({focused}) => (
          <TabBarIcon icon="profile" focused={focused} />
        ),
      },
    },
  },
  {
    tabBarOptions: tabBarOptions,
  },
);

const CoachTabs = createBottomTabNavigator(
  {
    Videos: {
      screen: CoachVideosStack,
      navigationOptions: {
        tabBarIcon: ({focused}) => (
          <TabBarIcon icon="videos" focused={focused} />
        ),
      },
    },
    Profile: {
      screen: CoachProfileStack,
      navigationOptions: {
        tabBarIcon: ({focused}) => (
          <TabBarIcon icon="profile" focused={focused} />
        ),
      },
    },
  },
  {
    tabBarOptions: tabBarOptions,
  },
);

const AthleteModalStack = createStackNavigator(
  {
    Main: AthleteTabs,
    AthleteNewVideo: AthleteNewVideoStack,
    ProfileEdit: ProfileEditStack,
    Account: AccountStack,
    VideoPlayer: VideoPlayerStack,
  },
  {
    mode: 'modal',
    headerMode: 'none',
  },
);

const CoachModalStack = createStackNavigator(
  {
    CoachMain: CoachTabs,
    CoachVideoFeedback: CoachFeedbackStack,
    CoachVideoFollowup: CoachFollowupStack,
    VideoPlayer: VideoPlayerScreen,
  },
  {
    mode: 'modal',
    headerMode: 'none',
  },
);

const AuthRegisterStack = createStackNavigator(
  {
    AuthRegisterAccount: AuthRegisterAccountScreen,
    AuthRegisterUserType: AuthRegisterUserTypeScreen,
    AuthRegisterProfile: AuthRegisterProfileScreen,
    AuthRegisterSubscription: AuthRegisterSubscriptionScreen,
  },
  {
    defaultNavigationOptions: navigationOptions,
  },
);

const AuthSwitcher = createSwitchNavigator(
  {
    AuthLogin: AuthLoginScreen,
    AuthRegister: AuthRegisterStack,
    AuthForgotPassword: AuthForgotPasswordScreen,
  },
  {
    defaultNavigationOptions: navigationOptions,
  },
);

const AppNavigator = createSwitchNavigator(
  {
    AuthDetection: AuthDetectionScreen,
    Onboarding: OnboardingScreen,
    Auth: AuthSwitcher,
    AthleteMain: AthleteModalStack,
    CoachMain: CoachModalStack,
  },
  {
    initialRouteName: 'AuthDetection',
  },
);

const AppContainer = createAppContainer(AppNavigator);

export default AppContainer;
