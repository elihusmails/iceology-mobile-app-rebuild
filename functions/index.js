const functions = require("firebase-functions");

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require("firebase-admin");
admin.initializeApp();

exports.sendFeedbackNotification = functions.firestore
  .document("videos/{videoId}/messages/{messageId}")
  .onCreate((doc, context) => {
    const videoId = context.params.videoId;

    const message = doc.data();
    let payload = {};

    switch (message.type) {
      case "COACH_FOLLOWUP":
      case "COACH_FEEDBACK":
        payload.notification = {
          body: `Iceology Pro replied to your video “${message.text}”`
        };
        break;
      default:
        return true;
    }

    return admin
      .firestore()
      .doc(`videos/${videoId}`)
      .get()
      .then(response => {
        const video = response.data();

        admin
          .firestore()
          .doc(`users/${video.uid}`)
          .get()
          .then(response => {
            const user = response.data();
            const token = user.messagingToken;

            return admin
              .messaging()
              .sendToDevice(token, payload)
              .then((response) => {
                console.log("Successfully sent notification to device:", payload, response);
              })
              .catch((error) => {
                console.log("Error sending notification to device:", payload, error);
              });
          })
          .catch((error) => {
            console.error(error);
          });
      })
      .catch((error) => {
        console.error(error);
      });
  });
