import React, { useState, useContext } from 'react'
import { Keyboard, View, Text, StyleSheet, TouchableOpacity } from 'react-native'

import { AuthContext } from "../../navigation/AuthProvider"
import FormButton from '../../components/FormButton'
import ContentWrapper from '../../components/ContentWrapper'
import LoginForm from '../../components/LoginForm'
import LoadingModal from "../../components/LoadingModal"
import Spacer from '../../components/Spacer'
import { H1, H3, TextButton } from "../../shared/typography"

const LoginScreen = ({ navigation }) => {
  const { login } = useContext(AuthContext)
  const [submitting, setSubmitting] = useState(null)

  const onSubmit = async (values) => {
    Keyboard.dismiss()

    try {
      setSubmitting(true)
      await login(values.email, values.password)
    } catch (error) {
      //setSubmitting(false)      
    } finally {
      setSubmitting(false)      
    }
  }
  
  return (
    <ContentWrapper
      navigationHeadroom
      type="KeyboardAwareScrollView"
      style={{
        flex: 1,
        justifyContent: "center",
      }}
    >
      <View style={{ flex: 2, justifyContent: "center" }}>
        <H1 style={{ textAlign: "center" }}>Log in</H1>
        <LoginForm onSubmit={onSubmit} />
        <LoadingModal visible={submitting} />
      </View>
  
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center", height: 120 }}>
        <Spacer height={12} />

        <TouchableOpacity onPress={() => navigation.navigate("ForgotPassword")}>
          <TextButton>Forgot password?</TextButton>
        </TouchableOpacity>
  
        <Spacer height={24} />
  
        <H3>New user?</H3>
        <TouchableOpacity onPress={() => navigation.navigate("Register")}>
          <TextButton>Register</TextButton>
        </TouchableOpacity>
      </View>
    </ContentWrapper>
  )
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f5f5f5',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  text: {
    fontSize: 24,
    marginBottom: 10
  },
  navButton: {
    marginTop: 15
  },
  navButtonText: {
    fontSize: 20,
    color: '#6646ee'
  }
})

export default LoginScreen