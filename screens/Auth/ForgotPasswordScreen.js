import React, { useState, useContext } from 'react'
import { Alert, Keyboard, View, Text, StyleSheet, TouchableOpacity } from 'react-native'

import { AuthContext } from "../../navigation/AuthProvider"
import FormButton from '../../components/FormButton'
import ContentWrapper from '../../components/ContentWrapper'
import ForgotPasswordForm from '../../components/ForgotPasswordForm'
import LoadingModal from "../../components/LoadingModal"
import Spacer from '../../components/Spacer'
import { H1, H3, TextButton } from "../../shared/typography"

const ForgotPasswordScreen = ({ navigation }) => {
  const { forgotPassword } = useContext(AuthContext)
  const [submitting, setSubmitting] = useState(null)

  const onSubmit = async (values) => {
    Keyboard.dismiss()

    try {
      setSubmitting(true)
      await forgotPassword(values.email)
      showSuccess()
    } catch (error) {
      //setSubmitting(false)      
    } finally {
      setSubmitting(false)      
    }
  }

  const showSuccess = () => {
    Alert.alert(
      'Success',
      'We emailed instructions to reset your password. Please follow the steps in the email, then come back here to log in.',
      [
        {
          text: 'OK',
          onPress: () => {
            navigation.popToTop()
            navigation.navigate('Login')
          },
        },
      ],
      {cancelable: false},
    )
  }
  
  return (
    <ContentWrapper
      navigationHeadroom
      type="KeyboardAwareScrollView"
      style={{
        flex: 1,
        justifyContent: "center",
      }}
    >
      <View style={{ flex: 2, justifyContent: "center" }}>
        <H1 style={{ textAlign: "center" }}>Forgot your password?</H1>
        <ForgotPasswordForm onSubmit={onSubmit} />
        <LoadingModal visible={submitting} />
      </View>
  
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center", height: 120 }}>
        <Spacer height={12} />

        <TouchableOpacity onPress={() => navigation.navigate("Login")}>
          <TextButton>Log in</TextButton>
        </TouchableOpacity>
      </View>
    </ContentWrapper>
  )
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f5f5f5',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  text: {
    fontSize: 24,
    marginBottom: 10
  },
  navButton: {
    marginTop: 15
  },
  navButtonText: {
    fontSize: 20,
    color: '#6646ee'
  }
})

export default ForgotPasswordScreen