import React, { useEffect, useLayoutEffect, useRef, useState } from 'react'
import _ from 'lodash'
import { Alert, AppState, Text, View } from 'react-native'
import { launchCamera, launchImageLibrary } from 'react-native-image-picker'
import Purchases from 'react-native-purchases'
import * as Sentry from '@sentry/react-native';

import {Platform} from 'react-native'
import {
  checkMultiple,
  request,
  requestMultiple,
  openSettings,
  PERMISSIONS,
  RESULTS,
} from 'react-native-permissions'

import { isSubscribed, getSubscriptionsUsage } from '../../../shared/purchases'
import { getObjectKeysByValue } from '../../../shared/utilities'
import { BodyText, ErrorText, TextButton } from '../../../shared/typography'

import Spacer from '../../../components/Spacer'
import PrimaryButton from '../../../components/Buttons'
import ContentWrapper from '../../../components/ContentWrapper'
//import LoadingModal from '../../../components/LoadingModal'
import NavigationButton from '../../../components/NavigationButtons'

function NewVideoScreen({ navigation, route }) {
  const appState = useRef(AppState.currentState)
  const mountedRef = useRef(false)
  const [appStateVisible, setAppStateVisible] = useState(appState.current)
  const [devicePermissionStatus, setDevicePermissionStatus] = useState(null)
  const [devicePermissions, setDevicePermissions] = useState({ camera: null, library: null})
  const [subscriptionStatus, setSubscriptionStatus] = useState(null)
  const [subscriptions, setSubscriptions] = useState(null)
  //const [loading, setLoading] = useState(false)

  // Update the device and subscription status whenever this screen gets focus
  useEffect(() => {
    mountedRef.current = true
    const unsubscribe = navigation.addListener('focus', () => {
      loadSubscriptionUsage()
      loadDevicePermissions()
    })

    return () => {
      mountedRef.current = false
      unsubscribe()
    }
  }, [navigation])

  useEffect(() => {
    console.log({ subscriptions })
    
    if (subscriptions === null || subscriptions.length === 0) {
      setSubscriptionStatus('NOT_SUBSCRIBED')
      return
    }
    
    const usedLessThanQuota = (subscription) => subscription.used < subscription.quota    
    const hasAnySubscriptionWithUnusedQuota = subscriptions.some(usedLessThanQuota)

    if (hasAnySubscriptionWithUnusedQuota) {
      setSubscriptionStatus('SUBSCRIBED')
    } else {
      setSubscriptionStatus('LIMIT')      
    }
    
  }, [subscriptions])

  useEffect(() => {
    AppState.addEventListener("change", handleAppStateChange)
    
    return () => {
      AppState.removeEventListener("change", handleAppStateChange)
    }
  }, [])

  // Update device permissions when coming back from iOS Settings
  const handleAppStateChange = (nextAppState) => {
    if (
      appState.current.match(/inactive|background/) &&
      nextAppState === "active"
    ) {
      loadDevicePermissions()
    }

    appState.current = nextAppState
    setAppStateVisible(appState.current)
  }

  const handleRecordPress = () => {
    navigation.navigate('AthleteNewVideoModal', { screen: 'AthleteNewVideoRecord' })
  }

  const handlePickerPress = () => {
    navigation.navigate('AthleteNewVideoModal', { screen: 'AthleteNewVideoPicker' })
  }

  const loadSubscriptionUsage = async () => {
    const subscribed = await isSubscribed()
    const usage = await getSubscriptionsUsage()
    //console.log({ usage })
    setSubscriptions(usage)
    
    ////const { used, quota } = subscriptionUsage
    //
    //if (subscribed) {
    //  if (used >= quota) {
    //    setSubscriptionStatus('LIMIT')
    //  } else {
    //    setSubscriptionStatus('SUBSCRIBED')
    //  }
    //} else {
    //  setSubscriptionStatus('NOT_SUBSCRIBED')
    //}
    
    //setLoading(false)
  }

  const loadDevicePermissions = async () => {
    let permissions

    if (Platform.OS === 'android') {
      await requestMultiple([
        PERMISSIONS.ANDROID.CAMERA,
        PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE
      ])

      permissions = await checkMultiple([
        PERMISSIONS.ANDROID.CAMERA,
        PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE,
      ])
      
      setDevicePermissions({
        camera:  permissions[PERMISSIONS.ANDROID.CAMERA],
        library: permissions[PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE],
      })
    }
    
    if (Platform.OS === 'ios') {
      await requestMultiple([
        PERMISSIONS.IOS.CAMERA,
        PERMISSIONS.IOS.MICROPHONE,
        PERMISSIONS.IOS.PHOTO_LIBRARY        
      ])
      
      permissions = await checkMultiple([
        PERMISSIONS.IOS.CAMERA,
        PERMISSIONS.IOS.MICROPHONE,
        PERMISSIONS.IOS.PHOTO_LIBRARY,
      ])
      
      setDevicePermissions({
        camera:  permissions[PERMISSIONS.IOS.CAMERA],
        library: permissions[PERMISSIONS.IOS.PHOTO_LIBRARY],
      })
    }
  }
  
  const renderLoadingSubscription = () => {
    const message = 'Loading subscription details…'

    return (
      <View>
        <BodyText style={{textAlign: 'center'}}>{message}</BodyText>
      </View>
    )
  }
  
  const renderSubscriptionRequired = () => {
    const message = 'You must have an active Iceology subscription to submit videos.'

    return (
      <View>
        <BodyText style={{textAlign: 'center'}}>{message}</BodyText>
        <Spacer height={8} />
        <TextButton onPress={() => navigation.navigate('AccountSubscription')}>Subscribe</TextButton>
      </View>
    )
  }
  
  const renderSubscriptionLimitReached = () => {
    const { used, quota } = subscriptionUsage
    const message = `You have reached your limit of ${quota} video${quota == 1 ? '' : 's'} in the current subscription period.`

    return (
      <View>
        <BodyText style={{textAlign: 'center'}}>{message}</BodyText>
        <Spacer height={8} />
        <TextButton onPress={() => navigation.navigate('Profile', { screen: 'AccountSubscription' })}>Manage Subscription</TextButton>
      </View>
    )
  }

  const renderRecordPermissionsDenied = () => {
    const message = Platform.select({
      android: 'Iceology needs access to your Camera.',
      ios: 'Iceology needs access to your Camera and Microphone.',
    });
    
    return (
      <View style={{marginVertical: 12}}>
        <BodyText style={{textAlign: 'center', lineHeight: 24}}>
          <Text style={{fontWeight: 'bold'}}>⚠️ To record a video</Text>, {message}
        </BodyText>
        <TextButton onPress={openSettings}>Open Settings</TextButton>
      </View>
    )
  }
  
  const renderPickerPermissionsDenied = () => {
    const message = Platform.select({
      android: 'Iceology needs access to your device Storage.',
      ios: 'Iceology needs access to your Photo Library.',
    });
    
    return (
      <View style={{marginVertical: 12}}>
        <BodyText style={{textAlign: 'center', lineHeight: 24}}>
          <Text style={{fontWeight: 'bold'}}>⚠️ To choose from your library</Text>, {message}
        </BodyText>
        <TextButton onPress={openSettings}>Open Settings</TextButton>
      </View>
    )
  }
  
  const renderRecordPermissionsUnavailable = () => {
    const message = Platform.select({
      android: 'Iceology needs access to your Camera',
      ios: 'Iceology needs access to your Camera and Microphone',
    });
    
    return (
      <View style={{marginVertical: 12}}>
        <BodyText style={{textAlign: 'center', lineHeight: 24}}>
          <Text style={{fontWeight: 'bold'}}>⚠️ To record a video</Text>, {message}, but something went wrong. Usually this means your device storage is full; try deleting some photos, videos, or apps.
        </BodyText>
      </View>
    )
  }
  
  const renderPickerPermissionsUnavailable = () => {
    const message = Platform.select({
      android: 'Iceology needs access to your device Storage',
      ios: 'Iceology needs access to your Photo Library',
    });
    
    return (
      <View style={{marginVertical: 12}}>
        <BodyText style={{textAlign: 'center', lineHeight: 24}}>
          <Text style={{fontWeight: 'bold'}}>⚠️ To choose from your library</Text>, {message}, but something went wrong. Usually this means your device storage is full; try deleting some photos, videos, or apps.
        </BodyText>
      </View>
    )
  }
  
  const renderRecordPermissionsUnknown = () => {
    const message = Platform.select({
      android: 'Iceology needs access to your Camera',
      ios: 'Iceology needs access to your Camera and Microphone',
    });
    
    return (
      <View style={{marginVertical: 12}}>
        <BodyText style={{textAlign: 'center', lineHeight: 24}}>
          <Text style={{fontWeight: 'bold'}}>⚠️ To record a video</Text>, {message}, but an unknown error occurred [{devicePermissions.camera}]
        </BodyText>
      </View>
    )
  }
  
  const renderPickerPermissionsUnknown = () => {
    const message = Platform.select({
      android: 'Iceology needs access to your device Storage',
      ios: 'Iceology needs access to your Photo Library',
    });
    
    return (
      <View style={{marginVertical: 12}}>
        <BodyText style={{textAlign: 'center', lineHeight: 24}}>
          <Text style={{fontWeight: 'bold'}}>⚠️ To choose from your library</Text>, {message}, but an unknown error occurred [{devicePermissions.library}]
        </BodyText>
      </View>
    )
  }

  const renderRecordPermissionsNeeded = () => {
    switch (devicePermissions.camera) {
      case 'granted':
      case 'limited':
        return;
        break;
      case 'denied':
      case 'blocked':
        return renderRecordPermissionsDenied()
        break;
      case 'unavailable':
        return renderRecordPermissionsUnavailable()
        break;
      default:
        return renderRecordPermissionsUnknown();
        break;
    }
  }
  
  const renderPickerPermissionsNeeded = () => {    
    switch (devicePermissions.library) {
      case 'granted':
      case 'limited':
        return;
        break;
      case 'denied':
      case 'blocked':
        return renderPickerPermissionsDenied();
        break;
      case 'unavailable':
        return renderPickerPermissionsUnavailable();
        break;
      default:
        return renderPickerPermissionsUnknown()      ;  
    }
  }
  
  const renderRecordButton = () => {
    const canHandleRecord = devicePermissions.camera == 'granted'

    return (
      <View style={{marginVertical: 12}}>
        <PrimaryButton onPress={handleRecordPress} text="Record a Video" disabled={!canHandleRecord} />
        {canHandleRecord || renderRecordPermissionsNeeded()}
      </View>
    )
  }
  
  const renderPickerButton = () => {
    const canHandlePicker = devicePermissions.library == 'granted' || devicePermissions.library == 'limited'

    return (
      <View style={{marginVertical: 12}}>
        <PrimaryButton onPress={handlePickerPress} text="Choose from Library" disabled={!canHandlePicker} />
        {canHandlePicker || renderPickerPermissionsNeeded()}
      </View>
    )
  }
  
  const renderNewVideoButtons = () => {
    //console.log({devicePermissions})

    return (
      <View>
        {renderRecordButton()}
        {renderPickerButton()}
        
        <View style={{marginVertical: 12}}>
          <BodyText style={{textAlign: 'center', fontStyle: 'italic'}}>
            Videos are limited to 60 seconds
          </BodyText>
        </View>
        
      </View>
    );    
  }
  
  const getRenderAction = () => {
    //console.log({subscriptionStatus})
    
    switch (subscriptionStatus) {
      case null: return renderLoadingSubscription(); break;
      case 'NOT_SUBSCRIBED': return renderSubscriptionRequired(); break;
      case 'LIMIT': return renderSubscriptionLimitReached(); break;
    }
    
    return renderNewVideoButtons()
  }

  return (
    <ContentWrapper style={{flex: 1}} type="View">
      <View style={{flex: 1, justifyContent: 'center'}}>
        {getRenderAction()}
        {/*<LoadingModal visible={loading} />*/}
      </View>
    </ContentWrapper>
  )
}

export default NewVideoScreen
