import React, { useContext, useLayoutEffect, useState } from 'react'
import {Alert, Button, Keyboard, Text, View} from 'react-native'

import { AuthContext } from '../../../navigation/AuthProvider'

import {requestVideoFollowup} from '../../../shared/actions'

import ContentWrapper from '../../../components/ContentWrapper'
import NavigationButton from '../../../components/NavigationButtons'
import LoadingModal from '../../../components/LoadingModal'
import AthleteVideoFollowupForm from '../../../components/AthleteVideoFollowupForm'

const AthleteVideoFollowupScreen = ({navigation, route}) => {
  const { user } = useContext(AuthContext)
  const { video } = route.params
  const [formData, setFormData] = useState({})
  const [submitting, setSubmitting] = useState(null)

  useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <NavigationButton
          onPress={() => { navigation.goBack() }}
        >Cancel</NavigationButton>
      )
    })
  }, [navigation])

  const handleFormSubmit = async formData => {
    Keyboard.dismiss()

    if (!formData) {
      return
    }
    
    try {
      setSubmitting(true)
      
      await requestVideoFollowup(user, video, formData.message, 'USER_FOLLOWUP_REQUEST')

      showSuccess()
    } catch (error) {
      console.log({error})
      showError(error)
    } finally {
      setSubmitting(false)      
    }
  }

  const showSuccess = () => {
    Alert.alert(
      'Success',
      'Your followup request was submitted.',
      [
        {
          text: 'OK',
          onPress: () => {
            navigation.popToTop()
            navigation.navigate('AthleteVideosList')
          },
        },
      ],
      {cancelable: false},
    )
  }

  const showError = error => {
    Alert.alert('Error', error, [{text: 'OK'}], {cancelable: false});
  }
  
  return (
    <ContentWrapper type="ScrollView">
    
      <AthleteVideoFollowupForm
        onSubmit={handleFormSubmit}
        initialValues={formData}
      />

      <LoadingModal visible={submitting} />
    </ContentWrapper>
  )
}

export default AthleteVideoFollowupScreen

//import React, {useState} from 'react'
//import {Keyboard} from 'react-native'
//import t from 'tcomb-form-native'
//import _ from 'lodash'
//
//import ContentWrapper from '../../../components/ContentWrapper'
//import PrimaryButton from '../../../components/Buttons'
//import LoadingModal from '../../../components/LoadingModal'
//
//import {updateVideoStatus, saveVideoMessage} from '../../../shared/actions'
//import {formStylesheet} from '../../../shared/forms'
//import {ErrorText} from '../../../shared/typography'
//
//const Form = t.form.Form
//
//const FormModel = t.struct({
//  message: t.String,
//})
// 
//let inputTextareaStylesheet = _.cloneDeep(formStylesheet)
//inputTextareaStylesheet.textbox.normal.height =
//  inputTextareaStylesheet.textbox.normal.height * 4.75
//inputTextareaStylesheet.textbox.error.height =
//  inputTextareaStylesheet.textbox.error.height * 4.75
//
//const formOptions = {
//  stylesheet: formStylesheet,
//  fields: {
//    message: {
//      label: 'Question or message to pro',
//      placeholder:
//        'What would you like your pro to follow up on? Please be as specific as possible.',
//      multiline: true,
//      numberOfLines: 6,
//      maxLength: 280,
//      stylesheet: inputTextareaStylesheet,
//      returnKeyType: 'done',
//      blurOnSubmit: true,
//    },
//  },
//}
//
//const VideoFollowupScreen = ({navigation, route}) => {
//  const [value, setValue] = useState({})
//  const [submitting, setSubmitting] = useState(false)
//  
//  const { video } = route.params
//
//  handleFormChange = (value) => {
//    setValue(value)
//  }
//
//  handleFormSubmit = async () => {
//    Keyboard.dismiss()
//
//    const formData = this.refs.form.getValue()
//    if (!formData) return
//
//    try {
//      this.setState({error: null, submitting: true})
//
//      await saveVideoMessage(
//        video.id,
//        formData.message,
//        'USER_FOLLOWUP_REQUEST',
//      )
//      await updateVideoStatus(video.id, 'AWAITING_FOLLOWUP')
//
//      navigation.dismiss()
//      navigation.navigate('AthleteVideosList')
//    } catch (error) {
//      this.setState({error: error, submitting: false})
//    }
//  }
//
//  return (
//    <ContentWrapper type="KeyboardAwareScrollView">
//      <Form
//        ref="form"
//        type={FormModel}
//        options={formOptions}
//        value={this.state.value}
//        disabled={submitting}
//        onChange={handleFormChange}
//      />
//
//      <PrimaryButton
//        onPress={handleFormSubmit}
//        disabled={submitting}
//        text={'Request Follow-up'}
//      />
//
//      {this.state.error && (
//        <>
//          <Spacer height={24} />
//          <ErrorText>{error}</ErrorText>
//        </>
//      )}
//
//      <LoadingModal visible={submitting} />
//    </ContentWrapper>
//  )
//}
