/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useLayoutEffect, useRef, useState } from 'react'
import _ from 'lodash'
import { Alert, AppState, Text, View } from 'react-native'
//import ImagePicker from 'react-native-image-picker'
import { launchCamera, launchImageLibrary } from 'react-native-image-picker'

import {Platform} from 'react-native'
import {
  //check,
  checkMultiple,
  //request,
  requestMultiple,
  openSettings,
  PERMISSIONS,
  RESULTS,
} from 'react-native-permissions'

import { BodyText, TextButton } from '../../../shared/typography'

import Spacer from '../../../components/Spacer'
import PrimaryButton from '../../../components/Buttons'
import ContentWrapper from '../../../components/ContentWrapper'
import LoadingModal from '../../../components/LoadingModal'
import NavigationButton from '../../../components/NavigationButtons'

function NewVideoPickerScreen({ navigation, route }) {
  const appState = useRef(AppState.currentState)
  const [appStateVisible, setAppStateVisible] = useState(appState.current)
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    openPicker()

    return () => {
      setLoading(false)
    }
  }, [])

  /*
  // BUG, TODO: checkDevicePermissions() no longer exists, as it was rolled into
  // NewVideoScreen.js, but we really *should* refactor that out so it can
  // also be used here.

  useEffect(() => {
    AppState.addEventListener("change", handleAppStateChange)

    return () => {
      AppState.removeEventListener("change", handleAppStateChange)
    }
  }, [])

  const handleAppStateChange = (nextAppState) => {
    if (
      appState.current.match(/inactive|background/) &&
      nextAppState === "active"
    ) {
      checkDevicePermissions()
    }

    appState.current = nextAppState
    setAppStateVisible(appState.current)
  }
  */

  const openPicker = () => {
    const options = {
      mediaType: 'video',
      videoQuality: 'high', // 'low', 'medium', or 'high' on iOS, 'low' or 'high' on Android
      durationLimit: 60,
      //saveToPhotos: true,
    }

    launchImageLibrary(options, (response) => {
      if (response.didCancel) {
        navigation.goBack()
      } else if (response.error) {
        showError(response.error);
      } else {
        navigation.navigate('AthleteNewVideoConfirm', {
          videoURI: response.uri,
        });
      }
    });
  }

  const showError = (error) => {
    Alert.alert('Error', error, [{text: 'OK'}], {cancelable: false});
  }

  return (
    <ContentWrapper type="View" style={{flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: '#000'}} />
  )
}

export default NewVideoPickerScreen
