import React from 'react';
import {Dimensions, Text, View} from 'react-native';
import Video from 'react-native-video';

import NavigationButton from '../components/NavigationButtons';

class VideoPlayerScreen extends React.Component {
  constructor(props) {
    super(props);

    //console.log(this.props.navigation.state.params.video);

    this.state = {
      video: this.props.navigation.state.params.video,
      loadingStatus: null,
    };
  }

  static navigationOptions = ({navigation}) => {
    return {
      //title: "Edit your profile",
      headerLeft: (
        <NavigationButton onPress={navigation.dismiss}>Close</NavigationButton>
      ),
    };
  };

  componentDidMount() {}

  //onBuffer(status) {
  //  console.log('onBuffer:');
  //  console.log(status);
  //}
  //
  //onError(error) {
  //  console.log('onError:');
  //  console.log(error);
  //}

  onLoadStart = () => {
    //console.log('onLoadStart');
    this.setState({loadingStatus: 'loading'});
  };

  onLoad = () => {
    this.setState({loadingStatus: 'loaded'});
  };

  render() {
    //console.log('VIDEO:');
    //console.log(this.state.video.video.downloadURL);
    return (
      <>
        <View style={containerStyles}>
          {this.state.video && (
            <Video
              source={{uri: this.state.video.video.downloadURL}}
              ref={ref => {
                this.player = ref;
              }}
              paused={false}
              resizeMode={'contain'}
              style={videoStyles}
              controls={true}
              //onBuffer={this.onBuffer} // Callback when remote video is buffering
              //onError={this.videoError} // Callback when video cannot be loaded
              onLoad={this.onLoad}
              onLoadStart={this.onLoadStart}
            />
          )}
          {this.state.loadingStatus === 'loading' && (
            <View
              style={{
                position: 'absolute',
                top: 0,
                right: 0,
                bottom: 0,
                left: 0,
                backgroundColor: 'black',
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={{color: 'white'}}>Loading video…</Text>
            </View>
          )}
        </View>
      </>
    );
  }
}

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const containerStyles = {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
};

const videoStyles = {
  aspectRatio: windowWidth / windowHeight,
  width: '100%',
  backgroundColor: 'black',
};

export default VideoPlayerScreen;
