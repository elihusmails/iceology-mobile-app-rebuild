import React from "react";
import { View } from "react-native";

import VideoMessages from "../components/VideoMessages.js";

export default class VideoFeedbackScreen extends React.Component {
  static navigationOptions = { title: "Feedback" };

  render() {
    const video = this.props.navigation.state.params.video;

    return <VideoMessages video={video} />;
  }
}
