import React, { useLayoutEffect } from 'react'

import Spacer from '../../../components/Spacer'
import ContentWrapper from '../../../components/ContentWrapper'
import VideoDetails from '../../../components/VideoDetails'
import VideoFeedback from '../../../components/VideoFeedback'
import {H2} from '../../../shared/typography'

const VideoDetailScreen = ({ navigation, route }) => {
  const { video } = route.params

  useLayoutEffect(() => {
    navigation.setOptions({
      title: 'Video Details'
    })
  }, [navigation])
    
  return (
    <ContentWrapper type="ScrollView">
      <H2>Video</H2>
      
      <VideoDetails video={video} navigation={navigation} />

      <Spacer height={16} />

      <H2>Feedback</H2>
      
      <VideoFeedback video={video} navigation={navigation} />
    </ContentWrapper>
  )
}

export default VideoDetailScreen