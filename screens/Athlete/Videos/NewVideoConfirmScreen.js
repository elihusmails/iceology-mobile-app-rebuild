import React, { useContext, useEffect, useLayoutEffect, useState } from 'react'
import {Alert, Dimensions, Keyboard, Text, View} from 'react-native'

import { AuthContext } from '../../../navigation/AuthProvider'

import {generateVideoThumbnail, saveVideo} from '../../../shared/actions'
import {contentPadding} from '../../../shared/constants'

import VideoConfirmForm from '../../../components/VideoConfirmForm'
import ContentWrapper from '../../../components/ContentWrapper'
import NavigationButton from '../../../components/NavigationButtons'
import Spacer from '../../../components/Spacer'
import VideoPreview from '../../../components/VideoPreview'
import VideoPreviewInline from '../../../components/VideoPreviewInline'
import ProgressModal from '../../../components/ProgressModal'

function NewVideoConfirmScreen({ navigation, route }) {
  const { user } = useContext(AuthContext)
  //const [video, setVideo] = useState(null)
  const [formData, setFormData] = useState({})
  const [thumbnail, setThumbnail] = useState(null)
  const [progress, setProgress] = useState(null)

  const { videoURI } = route.params

  useLayoutEffect(() => {
    navigation.setOptions({
      title: 'Review Video',
      headerLeft: () => (
        <NavigationButton
          onPress={() => {
            navigation.popToTop()
            navigation.goBack()
          }}
        >Cancel</NavigationButton>
      )
    })
  }, [navigation])

  useEffect(() => {
    processVideo()
    
    return () => {}
  }, [])

  const processVideo = async () => {
    const thumbnailURI = await generateVideoThumbnail(videoURI)
    setThumbnail(thumbnailURI ? 'file://' + thumbnailURI : null)
  }

  const handleFormSubmit = async formData => {
    Keyboard.dismiss()

    if (!formData) {
      return
    }

    try {
      await saveVideo(user, videoURI, formData, progress => {
        setProgress(progress)
      })
      showSuccess()
    } catch (error) {
      showError('There was a problem submitting your video. Please try again.')
      console.log({error})
    } finally {
      setProgress(null)      
    }
  }
  
  const showSuccess = () => {
    Alert.alert(
      'Success',
      'Your video was submitted! A Pro will reply to you shortly with feedback.',
      [
        {
          text: 'OK',
          onPress: () => {
            navigation.popToTop()
            navigation.navigate('AthleteVideosList')
          },
        },
      ],
      {cancelable: false},
    )
  }

  const showError = error => {
    Alert.alert('Error', error, [{text: 'OK'}], {cancelable: false});
  }

  const {width} = Dimensions.get('window')
  const progressModalVisible = progress !== null
  const progressModalText = progress !== null
    ? 'Uploading video: ' + Math.round(progress * 100) + '%'
    : ''

  return (
    <ContentWrapper type="ScrollView">

      <VideoPreviewInline videoURI={videoURI} />

      <Spacer height={16} />

      <VideoConfirmForm
        onSubmit={handleFormSubmit}
        initialValues={formData}
      />
      
      <ProgressModal
        //ref={ref => (this.progressModal = ref)}
        visible={progressModalVisible}
        text={progressModalText}
      />
    </ContentWrapper>
  )
}

export default NewVideoConfirmScreen
