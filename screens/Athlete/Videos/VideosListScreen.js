import React, { useContext, useEffect, useRef, useState } from 'react'
import {Alert, Image, View} from 'react-native'
import firebase from '@react-native-firebase/app'
import '@react-native-firebase/auth'
import SegmentedControl from '@react-native-segmented-control/segmented-control'

import { AuthContext } from '../../../navigation/AuthProvider'
import {firebaseGetUser, firebaseRequestMessagingPermission} from '../../../shared/firebase.js'
import {isUserProfileComplete} from '../../../shared/users.js'
import {H3} from '../../../shared/typography'

import ContentWrapper from '../../../components/ContentWrapper'
import Spacer from '../../../components/Spacer.js'
import VideosList from '../../../components/VideosList.js'
import PrimaryButton from '../../../components/Buttons'

const VideosListScreen = ({ navigation }) => {
  const { user } = useContext(AuthContext)
  const [videos, setVideos] = useState(null)
  const [videosUnsubscribe, setVideosUnsubscribe] = useState(null)
  const [currentView, setCurrentView] = useState('Active')

  const viewConfig = [
    {label: 'Active', statuses: ['AWAITING_FEEDBACK', 'AWAITING_FOLLOWUP', 'RECEIVED_FEEDBACK']},
    {label: 'Done',   statuses: ['COMPLETE']},
  ]

  useEffect(() => {
    // This isn't really working. Disable it for now.
    //firebaseRequestMessagingPermission()
  }, [])

  useEffect(
    () => {
      const statuses = viewConfig.find(o => o.label === currentView).statuses
      console.log({ statuses })

      const ref =
        firebase
          .firestore()
          .collection('videos')
          .where('status', 'in', statuses)
          .where('uid', '==', user.uid)
          .orderBy('createdAt', 'desc')
          .limit(50)

      const unsubscribe =
        ref.onSnapshot(snapshot => {
          let videos = []

          snapshot.forEach((doc) => {
            videos.push({id: doc.id, ...doc.data()})
          })

          setVideos(videos)
        },
        error => { console.log({error}) }
      )

      return () => unsubscribe()
    },
    [currentView]
  )

  const handleVideoPress = (video) => {
    navigation.navigate('AthleteVideoDetail', {video: video})
  }

  const EmptyState = (<ContentWrapper />)

  const NoVideos = (
    <View style={{flex: 2, justifyContent: 'center'}}>
      <H3 style={{textAlign: 'center'}}>
        There’s nothing here yet!
      </H3>
    </View>
  )

  const selectedIndex = viewConfig.findIndex(o => o.label === currentView)
  const values = viewConfig.map(o => o.label)
  console.log({videos})

  const Videos = videos && videos.length
    ? <VideosList
        videos={videos}
        handleVideoPress={handleVideoPress}
      />
    : NoVideos

  const Screen = (
    <ContentWrapper
      type="View"
      style={{
        flex: 1,
        paddingTop: 0,
        paddingRight: 0,
        paddingBottom: 0,
        paddingLeft: 0,
      }}>

      <View style={{ paddingHorizontal: 24, paddingTop:8, paddingBottom: 24, backgroundColor: '#FFFFFF' }}>
        <SegmentedControl
          appearance="light"
          values={viewConfig.map(o => o.label)}
          selectedIndex={viewConfig.findIndex(o => o.label === currentView)}
          onChange={(event) => {
            setCurrentView(event.nativeEvent.value)
          }}
        />
      </View>

      {Videos}

    </ContentWrapper>
  )

  return Screen
}

export default VideosListScreen
