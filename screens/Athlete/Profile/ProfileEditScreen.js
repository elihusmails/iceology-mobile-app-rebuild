import React, { useContext, useEffect, useLayoutEffect, useRef, useState } from 'react';
import {Alert, Keyboard} from 'react-native';

import firebase from '@react-native-firebase/app';
import '@react-native-firebase/auth';

import _ from 'lodash';

import { AuthContext } from '../../../navigation/AuthProvider'
import {
  firebaseGetUser,
  firebaseUpdateUserProfile,
  firebaseCurrentUser,
} from '../../../shared/firebase';

import ContentWrapper from '../../../components/ContentWrapper';
import NavigationButton from '../../../components/NavigationButtons';
import LoadingModal from '../../../components/LoadingModal';
import UserProfileForm from '../../../components/UserProfileForm';

const ProfileEditScreen = ({navigation}) => {
  const mountedRef = useRef(false)
  const { user } = useContext(AuthContext)
  const [profile, setProfile] = useState(null)
  const [loading, setLoading] = useState(false)

  useLayoutEffect(() => {
    navigation.setOptions({
      //title: 'Edit your Profile',
      headerLeft: () => (
        <NavigationButton onPress={() => navigation.goBack()}>Cancel</NavigationButton>
      )
    })
  }, [navigation]);


  useEffect(() => {
    mountedRef.current = true
    loadProfile()

    return () => {
      mountedRef.current = false
    }
  }, []) // Run on first render only

  const loadProfile = async () => {    
    try {
      const firebaseUser = await firebaseGetUser(user.uid)
      
      if (firebaseUser.profile) {
        if (mountedRef.current) {
          setProfile(firebaseUser.profile)
        }
      } else {
        setProfile({})
      }
    } catch (error) {
      console.log({error})
    } finally {
      if (mountedRef.current) {
        setLoading(false)
      }
    }
  }

  const handleSubmit = async (values) => {
    Keyboard.dismiss()
    setLoading(true)

    try {
      await firebaseUpdateUserProfile(user.uid, values)
      setLoading(false)      
      navigation.navigate('AthleteProfile')
    } catch (error) {
      setLoading(false)      
    }
  }

  return (
    <ContentWrapper type="KeyboardAwareScrollView">
      {profile && <UserProfileForm onSubmit={handleSubmit} initialValues={profile} />}
      <LoadingModal visible={loading} />
    </ContentWrapper>
  )

  showError = (error) => {
    Alert.alert('Error', error, [{text: 'OK'}], {cancelable: false});
  }
}

export default ProfileEditScreen;
