import React from 'react'

import { H3 } from '../../../shared/typography'

import ContentWrapper from '../../../components/ContentWrapper'
import Spacer from '../../../components/Spacer.js'
import Center from '../../../components/Center'
import SubscribeForm from '../../../components/Subscriptions/SubscribeForm'
import RestorePurchasesForm from '../../../components/Subscriptions/RestorePurchasesForm'
import SubscriptionTerms from '../../../components/Subscriptions/SubscriptionTerms'

const AccountSubscriptionScreen = ({ navigation }) => {  
  const handleSubscribe = () => {
    //Alert.alert('Success!', 'You are now subscribed!');
  }
  
  const handleRestorePurchases = () => {
    //Alert.alert('Success!', 'You are now subscribed!');
  }

  return (
    <ContentWrapper type="ScrollView">
      <H3 style={{textAlign: 'center'}}>
        Elevate your game with direct access to current and former hockey
        professionals
      </H3>

      <Spacer height={24} />

      <SubscribeForm onSuccess={handleSubscribe} />

      <Spacer height={12} />

      <RestorePurchasesForm onSuccess={handleRestorePurchases} />

      <Spacer height={24} />

      <SubscriptionTerms />

      <Spacer height={48} />
    </ContentWrapper>
  )
}

export default AccountSubscriptionScreen;
