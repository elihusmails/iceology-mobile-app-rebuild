import React, { useContext, useEffect, useLayoutEffect, useRef, useState } from 'react';
import {Alert, Button, Keyboard} from 'react-native';

import firebase from '@react-native-firebase/app';
import '@react-native-firebase/auth';

import { AuthContext } from '../../navigation/AuthProvider'
import ContentWrapper from '../../components/ContentWrapper';
import LoadingModal from '../../components/LoadingModal';
import UserProfileForm from '../../components/UserProfileForm';
import OnboardingCancelButton from '../../components/OnboardingCancelButton'

import { firebaseGetUser, firebaseUpdateUserProfile } from '../../shared/firebase.js';

const CreateProfileScreen = ({ navigation }) => {
  const mountedRef = useRef(false)
  const { user } = useContext(AuthContext)
  const [profile, setProfile] = useState(null)
  const [submitting, setSubmitting] = useState(false)
  const [loading, setLoading] = useState(false)

  useLayoutEffect(() => {
    navigation.setOptions({
      title: 'Create your profile',
      headerLeft: () => (
        <OnboardingCancelButton />
      )
    })
  }, [navigation]);

  useEffect(() => {
    mountedRef.current = true
    loadProfile()

    return () => {
      mountedRef.current = false
    }
  }, []) // Run on first render only

  const loadProfile = async () => {    
    try {
      const firebaseUser = await firebaseGetUser(user.uid)
      
      if (firebaseUser.profile) {
        if (mountedRef.current) {
          setProfile(firebaseUser.profile)
        }
      } else {
        setProfile({})
      }
    } catch (error) {
      console.log({error})
    } finally {
      if (mountedRef.current) {
        setLoading(false)
      }
    }
  }
  
  const handleSubmit = async (values) => {
    Keyboard.dismiss()
    setSubmitting(true)

    try {
      await firebaseUpdateUserProfile(user.uid, values)
      setSubmitting(false)      
      navigation.navigate('OnboardingSubscribe')
    } catch (error) {
      setSubmitting(false)      
    }
  }
  
  return (
    <ContentWrapper
      navigationHeadroom
      type="KeyboardAwareScrollView"
    >
      {profile && <UserProfileForm onSubmit={handleSubmit} initialValues={profile} />}
      <LoadingModal visible={loading || submitting} />
    </ContentWrapper>
  )
}

export default CreateProfileScreen