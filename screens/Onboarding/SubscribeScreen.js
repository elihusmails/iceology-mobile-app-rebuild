import React, { useContext, useEffect, useLayoutEffect, useState } from 'react'
import { Alert, Button, View } from 'react-native'
import moment from 'moment'
import Purchases from 'react-native-purchases'

import { getAvailablePackages } from '../../shared/purchases'
import { AuthContext } from '../../navigation/AuthProvider'

import {
  H3,
  ErrorText,
  BodyText,
  SmallText,
  TextButton,
} from '../../shared/typography'

import ContentWrapper from '../../components/ContentWrapper'
import Spacer from '../../components/Spacer.js'
import Center from '../../components/Center'
import LoadingModal from '../../components/LoadingModal'
import PrimaryButton from '../../components/Buttons'
import FormError from '../../components/FormError'
import OnboardingCancelButton from '../../components/OnboardingCancelButton'
import SubscriptionTerms from '../../components/Subscriptions/SubscriptionTerms'

const SubscribeScreen = ({ navigation }) => {
  const { saveOnboarded } = useContext(AuthContext)
  const [purchases, setPurchases] = useState(null)
  const [purchaserInfo, setPurchaserInfo] = useState(null)
  const [submitting, setSubmitting] = useState(null)
  const [error, setError] = useState(null)

  useEffect(() => {
    if (purchases === null) {
      initPurchases()
    }
  })
  
  useLayoutEffect(() => {  
    navigation.setOptions({
      title: 'Subscribe',
      headerLeft: () => (
        <OnboardingCancelButton />
      ),
    })
  }, [navigation])

  const initPurchases = async () => {
    try {
      const purchaser = await Purchases.getPurchaserInfo()
      //console.log({purchaser})
      setPurchaserInfo(purchaser)
            
      if (isSubscribed()) {
        navigation.setOptions({
          headerRight: () => (
            <Button onPress={() => saveOnboarded(true)} title="Next" />
          ),
        })
      }
      
      const availablePackages = await getAvailablePackages()
      if (availablePackages) {
        setPurchases(availablePackages)
      }
    } catch (error) {
      console.log({error})
    }
  }

  const handlePurchase = async (productPackage) => {
    setSubmitting(true)

    try {
      const { purchaserInfo, productIdentifier } = await Purchases.purchasePackage(productPackage)
      setPurchaserInfo(purchaserInfo)
      setSubmitting(false)
      saveOnboarded(true)
    } catch (e) {
      setSubmitting(false)
      if (!e.userCancelled) {
        Alert.alert('Error', e)
      }
    }
  }

  const handleRestorePurchases = async () => {
    setSubmitting(true)
    
    try {
      const restoredPurchaserInfo = await Purchases.restoreTransactions()
      Alert.alert('Success!', 'Your subscription has been restored');
      setPurchaserInfo(restoredPurchaserInfo)
      setSubmitting(false)
      saveOnboarded(true)
    } catch (e) {
      setSubmitting(false)
      if (!e.userCancelled) {
        Alert.alert('Error', e)
      }
    }

    // TODO: restore purchases

    // try {
    //   //console.log('Restoring purchases...')
    //   const info = await restorePurchases();
    //   //console.log(info)
    // 
    //   this.setState({submitting: false}, () => {
    //     this.props.navigation.navigate('Main')
    //   })
    // } catch (error) {
    //   this.setState({submitting: false, error: error}, () => {
    //     setTimeout(() => {
    //       Alert.alert('Error', error);
    //     }, 750);
    //   })
    // }
  }
  
  const isSubscribed = () => {
    if (!purchaserInfo) {
      return false
    }
    
    if (!purchaserInfo.activeSubscriptions) {
      return false
    }

    return purchaserInfo.activeSubscriptions.length > 0
  }
  
  const isSubscriptionActive = (productPackage) => {
    if (!purchaserInfo) {
      return false
    }

    if (!purchaserInfo.activeSubscriptions) {
      return false
    }
    
    return purchaserInfo.activeSubscriptions.indexOf(productPackage.product.identifier) !== -1
  }

  const renderPurchase = (productPackage) => {
    //const isActive = isActiveEntitlement(
    //  this.state.purchaseData.purchaserInfo,
    //  product.entitlementId,
    //);
    
    //const expirationDate = moment(
    //  getExpirationDate(
    //    this.state.purchaseData.purchaserInfo,
    //    product.entitlementId,
    //  ),
    //).format('LL');

    /*[
      {
        "identifier": "bronze", "offeringIdentifier": "default", "packageType": "CUSTOM",
        "product": {
          "currency_code": "USD", "description": "", "discounts": [Array], "identifier": "com.iceologyhockey.bronze.v2", "introPrice": null, "intro_price": null, "intro_price_cycles": null, "intro_price_period": null, "intro_price_period_number_of_units": null, "intro_price_period_unit": null, "intro_price_string": null, "price": 19.989999771118164, "price_string": "$19.99", "title": ""
        }
      },
      {
        "identifier": "silver", "offeringIdentifier": "default", "packageType": "CUSTOM",
        "product": {
          "currency_code": "USD", "description": "", "discounts": [Array], "identifier": "com.iceologyhockey.silver.v2", "introPrice": null, "intro_price": null, "intro_price_cycles": null, "intro_price_period": null, "intro_price_period_number_of_units": null, "intro_price_period_unit": null, "intro_price_string": null, "price": 39.9900016784668, "price_string": "$39.99", "title": ""
        }
      },
      {
        "identifier": "gold", "offeringIdentifier": "default", "packageType": "CUSTOM",
        "product": {
          "currency_code": "USD", "description": "", "discounts": [Array], "identifier": "com.iceologyhockey.gold.v2", "introPrice": null, "intro_price": null, "intro_price_cycles": null, "intro_price_period": null, "intro_price_period_number_of_units": null, "intro_price_period_unit": null, "intro_price_string": null, "price": 79.98999786376953, "price_string": "$79.99", "title": ""
        }
      }
    ]
  */

    return (
      <View key={productPackage.product.identifier.replace('.', '-')}>
        <PrimaryButton
          onPress={() => handlePurchase(productPackage)}
          disabled={submitting || isSubscriptionActive(productPackage)}
          text={`Subscribe to ${productPackage.product.title}`}
        />

        <Spacer height={8} />

        {/*isActive && (
          <View>
            <SmallText
              style={{
                textAlign: 'center',
                fontWeight: 'bold',
                marginBottom: 0,
              }}>
              Expires {expirationDate}
            </SmallText>
          </View>
        )*/}

        <BodyText style={{fontSize: 14, textAlign: 'center'}}>
          {productPackage.product.description} – {productPackage.product.price_string}/month
        </BodyText>

        <Spacer height={24} />
      </View>
    )
  }
  
  //console.log({ purchases })
  //console.log(purchaserInfo)

  return (
    <ContentWrapper type="ScrollView">
      <H3 style={{textAlign: 'center'}}>
        Elevate your game with direct access to current and former hockey
        professionals
      </H3>

      <FormError error={error} />

      <Spacer height={24} />

      {purchases && Array.isArray(purchases) && purchases.map(productPackage => renderPurchase(productPackage))}

      <Spacer height={12} />

      <Center>
        <TextButton onPress={handleRestorePurchases}>
          Restore Purchases
        </TextButton>
      </Center>

      <Spacer height={24} />

      <SubscriptionTerms />

      <LoadingModal visible={submitting} />
    </ContentWrapper>
  )  
}

export default SubscribeScreen