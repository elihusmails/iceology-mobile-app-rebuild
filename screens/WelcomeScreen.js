import React, { useContext } from 'react'
import { Dimensions, ImageBackground, StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import Swiper from 'react-native-swiper'
import FormButton from '../components/FormButton'
import { AuthContext } from '../navigation/AuthProvider'

import styled, { css } from "@emotion/native"

import { font } from "../shared/utilities"
import { contentPadding, darkBlue, textColor } from "../shared/constants"

import PrimaryButton from "../components/Buttons"

var { height } = Dimensions.get("window")

const SlideLayout = styled.View({
  paddingTop: height * 0.2,
  paddingBottom: contentPadding * 6,
  paddingHorizontal: contentPadding * 1.5,
  flex: 1,
  position: "absolute",
  width: "100%",
  height: "100%",
  justifyContent: "flex-start"
})

const SlideIcon = styled.Image({
  width: 72,
  height: 72,
  marginBottom: 8
})

const SlideTitle = styled.Text({
  ...font({
    weight: "Extrabold",
    size: 44, //size: "12vw",
    lineHeight: 1
  }),
  color: textColor,
  marginTop: 20,
  marginBottom: 0
})

const SlideText = styled.Text({
  ...font({
    weight: "Medium",
    size: 20,
    lineHeight: 1.25
  }),
  color: textColor,
  marginTop: 0,
  marginBottom: 24
})

const Dot = styled.View({
  backgroundColor: "rgba(0,0,0,.2)",
  width: 12,
  height: 12,
  borderRadius: 6,
  marginLeft: 6,
  marginRight: 6,
  marginTop: 0,
  marginBottom: 0
})

const ControlButton = styled.Text({
  ...font({
    size: 16,
    weight: "Regular"
  }),
  color: textColor,
  paddingVertical: 36,
  paddingHorizontal: contentPadding * 2,
  lineHeight: 24
})

const PrevButton = <View />
const NextButton = <ControlButton>Next</ControlButton>

const WelcomeSlide = ({ align, iconSource, backgroundSource, heading, text, button }) => {
  let justify = "flex-start"
  switch (align) {
    case "bottom":
      justify = "flex-end"
      break
    case "top":
    default:
      justify = "flex-start"
  }

  return (
    <ImageBackground
      style={styles.backgroundImage}
      source={backgroundSource}
    >
      <SlideLayout style={{ justifyContent: justify }}>
        <SlideIcon source={iconSource} />
        <SlideTitle>{heading}</SlideTitle>
        <SlideText>{text}</SlideText>
        {button}
      </SlideLayout>
    </ImageBackground>
  )
}

const WelcomeScreen = () => {
  const { welcomed, saveWelcomed } = useContext(AuthContext);

  return (
    <Swiper
      style={styles.wrapper}
      loop={false}
      activeDotColor={textColor}
      paginationStyle={{ marginBottom: 18 }}
      dot={<Dot />}
      activeDot={<Dot style={{ backgroundColor: textColor }} />}
      showsButtons={true}
      buttonWrapperStyle={{
        paddingHorizontal: 0,
        paddingVertical: 0,
        alignItems: "flex-end"
      }}
      prevButton={PrevButton}
      nextButton={NextButton}
    >
      <WelcomeSlide
        align="top"
        backgroundSource={require("../assets/images/onboarding-background-1.png")}
        iconSource={require("../assets/images/onboarding-icon-1.png")}
        heading={`Elevate\nYour Game`}
        text="Iceology gives you unprecedented access to current and former
            pro hockey players for feedback on your skills to help elevate
            your level of play."
      />

      <WelcomeSlide
        align="bottom"
        backgroundSource={require("../assets/images/onboarding-background-2.png")}
        iconSource={require("../assets/images/onboarding-icon-2.png")}
        heading={`Send`}
        text="It’s easy. Send your videos and questions to your pro and get
            expert, accurate feedback and advice."
      />

      <WelcomeSlide
        align="top"
        backgroundSource={require("../assets/images/onboarding-background-3.png")}
        iconSource={require("../assets/images/onboarding-icon-3.png")}
        heading={`Improve`}
        text="Your pro will send you detailed feedback. Enhance your skills by
            incorporating your personal coaching into your practice and game
            situations."
      />

      <WelcomeSlide
        align="bottom"
        backgroundSource={require("../assets/images/onboarding-background-4.png")}
        iconSource={require("../assets/images/onboarding-icon-4.png")}
        heading={`Win`}
        text="Use the professional feedback from your pro to help upgrade your
            overall game and become the impact player you want to be!"
      />

      <WelcomeSlide
        align="top"
        backgroundSource={require("../assets/images/onboarding-background-5.png")}
        iconSource={require("../assets/images/onboarding-icon-5.png")}
        heading={`Get Started`}
        text="Get started now and keep improving your game with Iceology."
        button={
          <PrimaryButton
            onPress={() => { saveWelcomed(true) }}
            text={"Get Started"}
          />
        }
      />
    </Swiper>
  )
}

const styles = StyleSheet.create({
  backgroundImage: {
    backgroundColor: darkBlue,
    flex: 1,
    resizeMode: "center",
    position: "absolute",
    width: "100%",
    height: "100%",
    justifyContent: "flex-start"
  }
})

export default WelcomeScreen
