import React, { useContext, useLayoutEffect, useState } from 'react'
import {Alert, Button, Keyboard, Text, View} from 'react-native'

import { AuthContext } from '../../../navigation/AuthProvider'

import {submitVideoFeedback} from '../../../shared/actions'

import ContentWrapper from '../../../components/ContentWrapper'
import NavigationButton from '../../../components/NavigationButtons'
import LoadingModal from '../../../components/LoadingModal'
import CoachVideoFeedbackForm from '../../../components/CoachVideoFeedbackForm'

const CoachVideoFeedbackScreen = ({navigation, route}) => {
  const { user } = useContext(AuthContext)
  const { video } = route.params
  const [formData, setFormData] = useState({})
  const [submitting, setSubmitting] = useState(null)

  useLayoutEffect(() => {
    navigation.setOptions({
      title: 'Feedback',
      headerLeft: () => (
        <NavigationButton
          onPress={() => { navigation.goBack() }}
        >Cancel</NavigationButton>
      )
    })
  }, [navigation])

  const handleFormSubmit = async formData => {
    Keyboard.dismiss()

    if (!formData) {
      return
    }
    
    try {
      setSubmitting(true)
      
      await submitVideoFeedback(user, video, formData.message, 'COACH_FEEDBACK')

      showSuccess('Your feedback was submitted.')
    } catch (error) {
      showError(error)
    } finally {
      setSubmitting(false)      
    }
  }

  const showSuccess = (text) => {
    Alert.alert(
      'Success',
      text,
      [
        {
          text: 'OK',
          onPress: () => {
            navigation.popToTop()
            navigation.navigate('CoachVideosList')
          },
        },
      ],
      {cancelable: false},
    )
  }

  const showError = error => {
    Alert.alert('Error', error, [{text: 'OK'}], {cancelable: false});
  }
  
  return (
    <ContentWrapper type="ScrollView">
    
      <CoachVideoFeedbackForm
        onSubmit={handleFormSubmit}
        initialValues={formData}
      />

      <LoadingModal visible={submitting} />
    </ContentWrapper>
  )
}

export default CoachVideoFeedbackScreen