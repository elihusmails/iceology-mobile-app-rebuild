import React, { useEffect, useLayoutEffect, useState } from 'react'

import { firebaseGetUser } from '../../../shared/firebase'

import Spacer from '../../../components/Spacer'
import ContentWrapper from '../../../components/ContentWrapper'
import VideoDetails from '../../../components/VideoDetails'
import VideoFeedback from '../../../components/CoachVideoFeedback'
import AthleteProfileCompact from '../../../components/AthleteProfileCompact'
import {H2} from '../../../shared/typography'

const VideoDetailScreen = ({ navigation, route }) => {
  const { video } = route.params
  const [ athlete, setAthlete ] = useState(null)

  useLayoutEffect(() => {
    navigation.setOptions({
      title: 'Video Details'
    })
  }, [navigation])
  
  useEffect(() => {
    firebaseGetUser(video.uid).then(user => {
      setAthlete(user)
      //console.log(user)
    })
  }, [video])
  
  return (
    <ContentWrapper type="ScrollView">
    
      <H2>Athlete</H2>
      {athlete && <AthleteProfileCompact profile={athlete.profile} />}

      <Spacer height={16} />
      
      <H2>Video</H2>
      <VideoDetails video={video} navigation={navigation} />

      <Spacer height={16} />

      <H2>Feedback</H2>
      <VideoFeedback video={video} navigation={navigation} />
      
    </ContentWrapper>
  )
}

export default VideoDetailScreen