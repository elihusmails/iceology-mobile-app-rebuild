import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';

import {Icon, Thumbnail} from 'native-base';

class ProfileSummary extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <TouchableOpacity
        onPress={() => this.props.navigation.navigate('Profile')}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
          }}>
          <View
            style={{
              flex: 1,
              flexDirection: 'column',
              justifyContent: 'center',
            }}>
            <Text style={{fontSize: 24, fontWeight: '500', marginBottom: 4}}>
              {this.props.profile.name}
            </Text>
            <Text style={{fontSize: 16}}>
              {[this.props.profile.position].filter(Boolean).join(', ')}
            </Text>
          </View>

          <View>
            <Icon
              type="Entypo"
              name="chevron-thin-right"
              style={{color: '#ccc'}}
            />
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

export default ProfileSummary;
