import React, { useContext, useEffect, useLayoutEffect, useRef, useState } from 'react'
import { AppState, View, Text } from 'react-native'

import firebase from '@react-native-firebase/app'
import '@react-native-firebase/auth'

import _ from 'lodash'

import { AuthContext } from '../../../navigation/AuthProvider'

import {H1, H2, BodyText, MediumText, SmallText, TextButton} from '../../../shared/typography'
import {textColor} from '../../../shared/constants'
import {
  firebaseGetUser,
  firebaseCurrentUser
} from '../../../shared/firebase'

import UserProfile from '../../../components/UserProfile';
import Center from '../../../components/Center';
import Spacer from '../../../components/Spacer';
import LoadingModal from '../../../components/LoadingModal';
import ContentWrapper from '../../../components/ContentWrapper';
import NavigationButton from '../../../components/NavigationButtons';

const ProfileScreen = ({ navigation }) => {
  const mountedRef = useRef(false)
  const appState = useRef(AppState.currentState)
  const [appStateVisible, setAppStateVisible] = useState(appState.current)
  const [loading, setLoading] = useState(false)
  const [profile, setProfile] = useState({})
  const { user, logout } = useContext(AuthContext)

  useEffect(() => {
    mountedRef.current = true
    const unsubscribe = navigation.addListener('focus', () => {
      loadProfile()
    })

    return () => {
      mountedRef.current = false
      unsubscribe()
    }
  }, [navigation])

  const loadProfile = async () => {    
    try {
      const firebaseUser = await firebaseGetUser(user.uid)
      //console.log({firebaseUser})
      
      if (firebaseUser.profile) {
        if (mountedRef.current) {
          setProfile(firebaseUser.profile)
        }
      }
    } catch (error) {
      console.log({error})
    } finally {
      if (mountedRef.current) {
        setLoading(false)
      }
    }
  }
  
  useEffect(() => {
    AppState.addEventListener("change", handleAppStateChange)
    
    return () => {
      AppState.removeEventListener("change", handleAppStateChange)
    }
  }, [])
  
  const handleAppStateChange = (nextAppState) => {
    if (
      appState.current.match(/inactive|background/) &&
      nextAppState === "active"
    ) {
      loadProfile()
    }
  
    appState.current = nextAppState
    setAppStateVisible(appState.current)
  }

  useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <NavigationButton onPress={() => navigation.navigate('CoachProfileEdit')}>
          Edit
        </NavigationButton>
      )
    })
  }, [navigation])

  const handleEditPress = async () => {
    navigation.navigate('AthleteProfileEdit')
  }

  const handleAccountPress = async () => {
    navigation.navigate('Account')
  }

  const handleLogOutPress = async () => {
    setLoading(true)
    await logout()
    setLoading(false)
  }

  return (
    <ContentWrapper type="ScrollView">
      <UserProfile profile={profile} />

      <Spacer height={16} />

      <H2>Account</H2>

      <TextButton align="left" onPress={handleLogOutPress}>Sign out</TextButton>

      <Spacer height={64} />

      <LoadingModal visible={loading} />
    </ContentWrapper>
  )
}

export default ProfileScreen
